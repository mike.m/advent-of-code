mod common;

const DEFAULT_PATH: &str = "./input/y2021d01s1.txt";

fn main() {
    let path = common::get_path_from_first_arg(DEFAULT_PATH);
    let content = common::read_file_as_lines(&*path);
    let steps = solve(content);
    println!("steps {}", steps);
}

fn solve(ocean_bottom: Vec<String>) -> i32 {
    let mut ob = convert_to_vec_of_chars(ocean_bottom);

    let mut i = 0;
    loop {
        let has_moved = all_one_step(&mut ob);
        i += 1;
        if !has_moved {
            return i;
        }
    }
}

fn convert_to_vec_of_chars(v: Vec<String>) -> Vec<Vec<char>> {
    return v
        .into_iter()
        .map(|l| l.chars().collect())
        .collect();
}

fn all_one_step(ocean_bottom: &mut Vec<Vec<char>>) -> bool {
    let size_y = ocean_bottom.len();
    let size_x = ocean_bottom[0].len();
    let mut x: usize;
    let mut y: usize;
    let mut moved = false;

    // Try to move left
    y = 0;
    while y < size_y {
        x = 0;
        let can_last_move = ocean_bottom[y][x] == '.';
        while x < size_x {
            let new_x = (x + 1) % size_x;
            if x == size_x - 1 && !can_last_move {
                break;
            }
            if ocean_bottom[y][x] == '>' && ocean_bottom[y][new_x] == '.' {
                ocean_bottom[y][x] = '.';
                ocean_bottom[y][new_x] = '>';
                // println!("move right: ({},{})->({},{})", x, y, new_x, y);
                moved = true;
                x += 1;
            }
            x += 1;
        }
        y += 1;
    }

    // Try to move down
    x = 0;
    while x < size_x {
        y = 0;
        let can_last_move = ocean_bottom[y][x] == '.';
        while y < size_y {
            if y == size_y - 1 && !can_last_move {
                break;
            }
            let new_y = (y + 1) % size_y;
            if ocean_bottom[y][x] == 'v' && ocean_bottom[new_y][x] == '.' {
                ocean_bottom[y][x] = '.';
                ocean_bottom[new_y][x] = 'v';
                // println!("move down: ({},{})->({},{})", x, y, x, new_y);
                moved = true;
                y += 1;
            } else {
                // println!("NOT move down: ({},{})->({},{})", x, y, x, new_y);
            }
            y += 1;
        }
        x += 1;
    }

    moved
}

#[cfg(test)]
mod tests {
    use crate::{all_one_step, convert_to_vec_of_chars, solve};

    #[test]
    fn all_simple_cases() {
        struct TestCase {
            name: &'static str,
            init: Vec<String>,
            want: Vec<String>,
            moved: bool,
        }

        let test_cases = vec![
            TestCase {
                name: "horizontal dots",
                init: vec!["...".to_string()],
                want: vec!["...".to_string()],
                moved: false,
            },
            TestCase {
                name: "horizontal left",
                init: vec![">..".to_string()],
                want: vec![".>.".to_string()],
                moved: true,
            },
            TestCase {
                name: "horizontal middle",
                init: vec![".>.".to_string()],
                want: vec!["..>".to_string()],
                moved: true,
            },
            TestCase {
                name: "horizontal right",
                init: vec!["..>".to_string()],
                want: vec![">..".to_string()],
                moved: true,
            },
            TestCase {
                name: "horizontal left middle",
                init: vec![">>.".to_string()],
                want: vec![">.>".to_string()],
                moved: true,
            },
            TestCase {
                name: "horizontal middle right",
                init: vec![".>>".to_string()],
                want: vec![">>.".to_string()],
                moved: true,
            },
            TestCase {
                name: "horizontal left right",
                init: vec![">.>".to_string()],
                want: vec![".>>".to_string()],
                moved: true,
            },
            TestCase {
                name: "horizontal left middle right",
                init: vec![">>>".to_string()],
                want: vec![">>>".to_string()],
                moved: false,
            },
            TestCase {
                name: "vertical top",
                init: vec![
                    "v".to_string(),
                    ".".to_string(),
                    ".".to_string(),
                ],
                want: vec![
                    ".".to_string(),
                    "v".to_string(),
                    ".".to_string(),
                ],
                moved: true,
            },
            TestCase {
                name: "vertical middle",
                init: vec![
                    ".".to_string(),
                    "v".to_string(),
                    ".".to_string(),
                ],
                want: vec![
                    ".".to_string(),
                    ".".to_string(),
                    "v".to_string(),
                ],
                moved: true,
            },
            TestCase {
                name: "vertical bottom",
                init: vec![
                    ".".to_string(),
                    ".".to_string(),
                    "v".to_string(),
                ],
                want: vec![
                    "v".to_string(),
                    ".".to_string(),
                    ".".to_string(),
                ],
                moved: true,
            },
            TestCase {
                name: "vertical top middle",
                init: vec![
                    "v".to_string(),
                    "v".to_string(),
                    ".".to_string(),
                ],
                want: vec![
                    "v".to_string(),
                    ".".to_string(),
                    "v".to_string(),
                ],
                moved: true,
            },
            TestCase {
                name: "vertical middle bottom",
                init: vec![
                    ".".to_string(),
                    "v".to_string(),
                    "v".to_string(),
                ],
                want: vec![
                    "v".to_string(),
                    "v".to_string(),
                    ".".to_string(),
                ],
                moved: true,
            },
            TestCase {
                name: "vertical top bottom",
                init: vec![
                    "v".to_string(),
                    ".".to_string(),
                    "v".to_string(),
                ],
                want: vec![
                    ".".to_string(),
                    "v".to_string(),
                    "v".to_string(),
                ],
                moved: true,
            },
            TestCase {
                name: "vertical all",
                init: vec![
                    "v".to_string(),
                    "v".to_string(),
                    "v".to_string(),
                ],
                want: vec![
                    "v".to_string(),
                    "v".to_string(),
                    "v".to_string(),
                ],
                moved: false,
            },
            TestCase {
                name: "vertical all",
                init: vec![
                    ".".to_string(),
                    ".".to_string(),
                    ".".to_string(),
                ],
                want: vec![
                    ".".to_string(),
                    ".".to_string(),
                    ".".to_string(),
                ],
                moved: false,
            },
        ];

        for tc in test_cases {
            let mut ob = convert_to_vec_of_chars(tc.init);
            let moved = all_one_step(&mut ob);
            let want = convert_to_vec_of_chars(tc.want);
            assert_eq!(ob, want, "test failed: invalid position: {}", tc.name);
            assert_eq!(moved, tc.moved, "test failed: invalid moved flag: {}", tc.name);
        }
    }

    fn print_it(rows: &Vec<Vec<char>>) {
        for row in rows {
            for ch in row {
                print!("{}", ch);
            }
            println!();
        }
        println!("-----------------");
    }

    #[test]
    fn solve_test_case() {
        let ob = vec![
            "v...>>.vv>".to_string(),
            ".vv>>.vv..".to_string(),
            ">>.>v>...v".to_string(),
            ">>v>>.>.v.".to_string(),
            "v>v.vv.v..".to_string(),
            ">.>>..v...".to_string(),
            ".vv..>.>v.".to_string(),
            "v.v..>>v.v".to_string(),
            "....v..v.>".to_string(),
        ];

        let moves = solve(ob);

        assert_eq!(moves, 58);
    }

    #[test]
    fn four_steps() {
        // Initial state
        let mut init = vec![
            "...>...".to_string(),
            ".......".to_string(),
            "......>".to_string(),
            "v.....>".to_string(),
            "......>".to_string(),
            ".......".to_string(),
            "..vvv..".to_string(),
        ];
        let mut ob = convert_to_vec_of_chars(init);

        all_one_step(&mut ob);
        print_it(&ob);

        // Step 1
        assert_eq!(convert_to_vec_of_chars(vec![
            "..vv>..".to_string(),
            ".......".to_string(),
            ">......".to_string(),
            "v.....>".to_string(),
            ">......".to_string(),
            ".......".to_string(),
            "....v..".to_string(),
        ]), ob);

        all_one_step(&mut ob);
        print_it(&ob);

        // Step 2
        assert_eq!(convert_to_vec_of_chars(vec![
            "....v>.".to_string(),
            "..vv...".to_string(),
            ".>.....".to_string(),
            "......>".to_string(),
            "v>.....".to_string(),
            ".......".to_string(),
            ".......".to_string(),
        ]), ob);

        all_one_step(&mut ob);
        print_it(&ob);

        // Step 3
        assert_eq!(convert_to_vec_of_chars(vec![
            "......>".to_string(),
            "..v.v..".to_string(),
            "..>v...".to_string(),
            ">......".to_string(),
            "..>....".to_string(),
            "v......".to_string(),
            ".......".to_string(),
        ]), ob);

        all_one_step(&mut ob);
        print_it(&ob);

        // Step 4
        assert_eq!(convert_to_vec_of_chars(vec![
            ">......".to_string(),
            "..v....".to_string(),
            "..>.v..".to_string(),
            ".>.v...".to_string(),
            "...>...".to_string(),
            ".......".to_string(),
            "v......".to_string(),
        ]), ob);
    }

    #[test]
    fn test_case_58() {
        #[derive(Clone)]
        struct TestCase {
            step: i32,
            state: Vec<Vec<char>>,
        }
        // impl Clone for TestCase {
        //     fn clone(&self) -> Self {
        //         todo!()
        //     }
        // }
        // impl Copy for TestCase {
        //
        // }

        let mut want = vec![
            TestCase {
                step: 1,
                state: convert_to_vec_of_chars(vec![
                    "....>.>v.>".to_string(),
                    "v.v>.>v.v.".to_string(),
                    ">v>>..>v..".to_string(),
                    ">>v>v>.>.v".to_string(),
                    ".>v.v...v.".to_string(),
                    "v>>.>vvv..".to_string(),
                    "..v...>>..".to_string(),
                    "vv...>>vv.".to_string(),
                    ">.v.v..v.v".to_string(),
                ]),
            },
            TestCase {
                step: 2,
                state: convert_to_vec_of_chars(vec![
                    ">.v.v>>..v".to_string(),
                    "v.v.>>vv..".to_string(),
                    ">v>.>.>.v.".to_string(),
                    ">>v>v.>v>.".to_string(),
                    ".>..v....v".to_string(),
                    ".>v>>.v.v.".to_string(),
                    "v....v>v>.".to_string(),
                    ".vv..>>v..".to_string(),
                    "v>.....vv.".to_string(),
                ]),
            },
            TestCase {
                step: 3,
                state: convert_to_vec_of_chars(vec![
                    "v>v.v>.>v.".to_string(),
                    "v...>>.v.v".to_string(),
                    ">vv>.>v>..".to_string(),
                    ">>v>v.>.v>".to_string(),
                    "..>....v..".to_string(),
                    ".>.>v>v..v".to_string(),
                    "..v..v>vv>".to_string(),
                    "v.v..>>v..".to_string(),
                    ".v>....v..".to_string(),
                ]),
            },
            TestCase {
                step: 4,
                state: convert_to_vec_of_chars(vec![
                    "v>..v.>>..".to_string(),
                    "v.v.>.>.v.".to_string(),
                    ">vv.>>.v>v".to_string(),
                    ">>.>..v>.>".to_string(),
                    "..v>v...v.".to_string(),
                    "..>>.>vv..".to_string(),
                    ">.v.vv>v.v".to_string(),
                    ".....>>vv.".to_string(),
                    "vvv>...v..".to_string(),
                ]),
            },
            TestCase {
                step: 5,
                state: convert_to_vec_of_chars(vec![
                    "vv>...>v>.".to_string(),
                    "v.v.v>.>v.".to_string(),
                    ">.v.>.>.>v".to_string(),
                    ">v>.>..v>>".to_string(),
                    "..v>v.v...".to_string(),
                    "..>.>>vvv.".to_string(),
                    ".>...v>v..".to_string(),
                    "..v.v>>v.v".to_string(),
                    "v.v.>...v.".to_string(),
                ]),
            },
            TestCase {
                step: 10,
                state: convert_to_vec_of_chars(vec![
                    "..>..>>vv.".to_string(),
                    "v.....>>.v".to_string(),
                    "..v.v>>>v>".to_string(),
                    "v>.>v.>>>.".to_string(),
                    "..v>v.vv.v".to_string(),
                    ".v.>>>.v..".to_string(),
                    "v.v..>v>..".to_string(),
                    "..v...>v.>".to_string(),
                    ".vv..v>vv.".to_string(),
                ]),
            },
            TestCase {
                step: 20,
                state: convert_to_vec_of_chars(vec![
                    "v>.....>>.".to_string(),
                    ">vv>.....v".to_string(),
                    ".>v>v.vv>>".to_string(),
                    "v>>>v.>v.>".to_string(),
                    "....vv>v..".to_string(),
                    ".v.>>>vvv.".to_string(),
                    "..v..>>vv.".to_string(),
                    "v.v...>>.v".to_string(),
                    "..v.....v>".to_string(),
                ]),
            },
            TestCase {
                step: 30,
                state: convert_to_vec_of_chars(vec![
                    ".vv.v..>>>".to_string(),
                    "v>...v...>".to_string(),
                    ">.v>.>vv.>".to_string(),
                    ">v>.>.>v.>".to_string(),
                    ".>..v.vv..".to_string(),
                    "..v>..>>v.".to_string(),
                    "....v>..>v".to_string(),
                    "v.v...>vv>".to_string(),
                    "v.v...>vvv".to_string(),
                ]),
            },
            TestCase {
                step: 40,
                state: convert_to_vec_of_chars(vec![
                    ">>v>v..v..".to_string(),
                    "..>>v..vv.".to_string(),
                    "..>>>v.>.v".to_string(),
                    "..>>>>vvv>".to_string(),
                    "v.....>...".to_string(),
                    "v.v...>v>>".to_string(),
                    ">vv.....v>".to_string(),
                    ".>v...v.>v".to_string(),
                    "vvv.v..v.>".to_string(),
                ]),
            },
            TestCase {
                step: 0,
                state: convert_to_vec_of_chars(vec![
                    "..>>v>vv.v".to_string(),
                    "..v.>>vv..".to_string(),
                    "v.>>v>>v..".to_string(),
                    "..>>>>>vv.".to_string(),
                    "vvv....>vv".to_string(),
                    "..v....>>>".to_string(),
                    "v>.......>".to_string(),
                    ".vv>....v>".to_string(),
                    ".>v.vv.v..".to_string(),
                ]),
            },
            TestCase {
                step: 55,
                state: convert_to_vec_of_chars(vec![
                    "..>>v>vv..".to_string(),
                    "..v.>>vv..".to_string(),
                    "..>>v>>vv.".to_string(),
                    "..>>>>>vv.".to_string(),
                    "v......>vv".to_string(),
                    "v>v....>>v".to_string(),
                    "vvv...>..>".to_string(),
                    ">vv.....>.".to_string(),
                    ".>v.vv.v..".to_string(),
                ]),
            },
            TestCase {
                step: 56,
                state: convert_to_vec_of_chars(vec![
                    "..>>v>vv..".to_string(),
                    "..v.>>vv..".to_string(),
                    "..>>v>>vv.".to_string(),
                    "..>>>>>vv.".to_string(),
                    "v......>vv".to_string(),
                    "v>v....>>v".to_string(),
                    "vvv....>.>".to_string(),
                    ">vv......>".to_string(),
                    ".>v.vv.v..".to_string(),
                ]),
            },
            TestCase {
                step: 57,
                state: convert_to_vec_of_chars(vec![
                    "..>>v>vv..".to_string(),
                    "..v.>>vv..".to_string(),
                    "..>>v>>vv.".to_string(),
                    "..>>>>>vv.".to_string(),
                    "v......>vv".to_string(),
                    "v>v....>>v".to_string(),
                    "vvv.....>>".to_string(),
                    ">vv......>".to_string(),
                    ".>v.vv.v..".to_string(),
                ]),
            },
            TestCase {
                step: 58,
                state: convert_to_vec_of_chars(vec![
                    "..>>v>vv..".to_string(),
                    "..v.>>vv..".to_string(),
                    "..>>v>>vv.".to_string(),
                    "..>>>>>vv.".to_string(),
                    "v......>vv".to_string(),
                    "v>v....>>v".to_string(),
                    "vvv.....>>".to_string(),
                    ">vv......>".to_string(),
                    ".>v.vv.v..".to_string(),
                ]),
            },
        ];

        let mut ob = convert_to_vec_of_chars(vec![
            "v...>>.vv>".to_string(),
            ".vv>>.vv..".to_string(),
            ">>.>v>...v".to_string(),
            ">>v>>.>.v.".to_string(),
            "v>v.vv.v..".to_string(),
            ">.>>..v...".to_string(),
            ".vv..>.>v.".to_string(),
            "v.v..>>v.v".to_string(),
            "....v..v.>".to_string(),
        ]);
        print_it(&ob);
        let mut want_i = 0;
        let mut i = 0;
        loop {
            let has_moved = all_one_step(&mut ob);
            i += 1;
            println!("After step {}:", i);
            print_it(&ob);
            let TestCase { step: want_step, state: want_state } = want[want_i].clone();
            if want_step == i {
                let cc = ob.clone();
                assert_eq!(want_state, cc, "test step {}", i);
                want_i += 1;
            }
            if !has_moved {
                break;
            }
        }
    }
}
