extern crate core;

use core::panicking::panic;
use std::env;

mod common;

const DEFAULT_PATH: &str = "./input/y2021d02s1.txt";
const MAX_MODEL_NUMBER_SIZE: usize = 14;

fn main() {
    let path = common::get_path_from_first_arg(DEFAULT_PATH);
    let content = common::read_file_as_lines(&*path);
    let steps = solve(content);
    println!("{}", steps)
}

struct Storage {
    w: i32,
    x: i32,
    y: i32,
    z: i32,
}

impl Storage {
    fn set(&mut self, var: char, value: i32) {
        match var {
            'w' => self.w = value,
            'x' => self.x = value,
            'y' => self.y = value,
            'z' => self.z = value,
            _ => panic!("invalid variable: {}", var),
        }
    }

    fn get(&self, var: char) -> i32 {
        return match var {
            'w' => self.w,
            'x' => self.x,
            'y' => self.y,
            'z' => self.z,
            _ => panic!("invalid variable: {}", var),
        };
    }
}

struct Validator {
    storage: Storage,

}

impl Validator {
    fn is_valid(model_number: &str) -> bool {
        if model_number.len() != MAX_MODEL_NUMBER_SIZE {
            return false;
        }
        let numbers = model_number.chars();
        false
    }
}

enum Operand {
    Variable(char),
    Value(i32),
}

impl Operand {
    fn from(s: &str) -> Operand {
        let try_to_convert_to_number = s.parse::<i32>();
        return match try_to_convert_to_number {
            Ok(number) => Operand::Value(number),
            Err(_) => {
                if s.len() == 1 {
                    Operand::Variable(s.chars()[0])
                } else {
                    panic!("invalid operand:")
                }
            }
        };
    }
}

enum Command {
    Input(char, usize),
    Add(char, Operand),
    Subtract(char, Operand),
    Multiply(char, Operand),
    Divide(char, Operand),
    Modulo(char, Operand),
    Equals(char, Operand),
}

impl Command {
    fn from(strings: Vec<String>, index: usize) -> Command {
        if strings.len() < 2 {
            panic!("not enough strings to make a command: {}", strings.len())
        }
        let operation = strings[0].as_str();
        let var = strings[1].chars().last().expect(format!("invalid var: {}", strings[1]).as_str());
        match operation {
            "inp" => Command::Input(var, index),
            "add" => {
                Command::Add(var, Operand::from(strings[2].as_str()))
            }
            "sub" => {
                Command::Subtract(var, Operand::from(strings[2].as_str()))
            }
            "mul" => {
                Command::Multiply(var, Operand::from(strings[2].as_str()))
            }
            "div" => {
                Command::Divide(var, Operand::from(strings[2].as_str()))
            }
            "mod" => {
                Command::Modulo(var, Operand::from(strings[2].as_str()))
            }
            "eql" => {
                Command::Equals(var, Operand::from(strings[2].as_str()))
            }
            _ => panic!("invalid command: {}", string[0])
        }

        0
    }
}

struct Engine {
    commands: Vec<Command>,
}

impl Engine {
    fn from(strings: Vec<String>) {
        let mut index: usize = 0;
        for s in strings {
            let chunks: Vec<String> = s.split(" ").collect();
            if chunks.len() < 2 {
                panic!("wrong number of chunks: {}", chunks.len())
            }
            let op = Command::from(chunks);
        }
    }
}

fn is_model_number_valid(validator: &Validator) -> i32 {
    commands.into_iter().for_each(|l| println!("{}", l));
    let s = Storage { w: 0, x: 0, y: 0, z: 0 };
    let v = Validator { storage: s };

    0
}

#[cfg(test)]
mod tests {
    #[test]
    fn just_pass() {}
}
