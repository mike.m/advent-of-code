use std::{env, fs, io};
use std::io::BufRead;

/*pub fn read_file_as_line(path: &str) -> String {
    fs::read_to_string(path).expect(format!("failed: {}", path).as_str())
}*/

pub fn read_file_as_lines(path: &str) -> Vec<String> {
    let f = fs::File::open(path).expect(format!("failed: {}", path).as_str());
    let l = io::BufReader::new(f).lines();
    let ll: Vec<String> = l.map(|l| l.expect("Could not parse line")).collect();
    ll
}

pub fn get_path_from_first_arg(default_path: &str) -> &str {
    let args: Vec<String> = env::args().collect();
    let dp = String::from(default_path);
    let path = args.get(1).unwrap_or(&dp);
    path
}
