package main

import (
	"fmt"
	"mike/aoc/2018/inpututils"
)

func main() {
	lines := inpututils.GetLinesFromFile("../star03-input.txt")
	doubleInput := make(chan string)
	tripleInput := make(chan string)
	result := make(chan int)
	go countLinesFor(doubleInput, result, 2)
	go countLinesFor(tripleInput, result, 3)
	for _, s := range lines {
		if s == "" {
			continue
		}
		doubleInput <- s
		tripleInput <- s
	}
	doubleInput <- ""
	tripleInput <- ""
	first := <-result
	second := <-result
	checksum := first * second
	fmt.Printf("Checksum is %d * %d = %d", first, second, checksum)
}

func countLinesFor(input chan string, result chan int, howMany int) {
	counter := 0
	for {
		s := <-input
		if s == "" {
			break
		}
		if countFor(s, howMany) {
			counter++
		}
	}
	result <- counter
}

func countFor(line string, howMany int) bool {
	letters := map[rune]int{}
	for _, ch := range line {
		value, exists := letters[ch]
		if exists {
			letters[ch] = 1 + value
		} else {
			letters[ch] = 1
		}
	}
	for _, value := range letters {
		if value == howMany {
			return true
		}
	}
	return false
}
