package main

import(
	"fmt"
	"strconv"
	"log"
	"mike/aoc/2018/inpututils"
)

func main() {
	fmt.Printf("Final frequency %d", calculateFrequency())
}

func calculateFrequency() int {
	frequency := 0
	changes := inpututils.GetLinesFromFile("star01-input.txt")
	for _, changeText := range changes {
		change, err := strconv.Atoi(changeText)
		if err == nil {
			frequency = frequency + change
		} else {
			log.Fatal("not a number", changeText)
		}
	}
	return frequency
}
