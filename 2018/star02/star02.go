package main

import (
	"log"
	"mike/aoc/2018/inpututils"
	"strconv"
	"fmt"
)

func main() {
	lines := inpututils.GetLinesFromFile("../star01/star01-input.txt")
	fmt.Printf ("Duplicated frequency %d", calculateFrequency(lines))
}

func calculateFrequency(changes []string) int {
	frequency := 0
	register := make(map[int]int, 0)
	register[0] = 1
	for i := 0; i < 1000; i++ {
		for _, changeText := range changes {
			change, err := strconv.Atoi(changeText)
			if err == nil {
				frequency = frequency + change
				_, exist := register[frequency]
				if exist {
					return frequency
				} else {
					register[frequency] = 0
				}
			} else {
				log.Fatal("not a number", changeText)
			}
		}
	}
	log.Fatal("No duplicate frequency found")
	panic("No duplicate frequency found")
}
