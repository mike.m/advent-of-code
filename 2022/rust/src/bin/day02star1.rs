use crate::Hand::{Paper, Rock, Scissors};

mod common;

const DEFAULT_PATH: &str = "./input/input-day02star1.txt";

fn main() {
    let path = common::get_path_from_first_arg(DEFAULT_PATH);
    let content = common::read_file_as_lines(&*path);
    let score = solve(content);
    println!("score {}", score);
}

enum Hand {
    Rock,
    Paper,
    Scissors,
}

impl Hand {
    fn from_string(hand: char) -> Self {
        match hand {
            'A' | 'X' => Rock,
            'B' | 'Y' => Paper,
            'C' | 'Z' => Scissors,
            _ => panic!("invalid letter `{}`", hand),
        }
    }

    /**
    Returns the score.

    For selected hand:
    - Rock +1
    - Paper +2
    - Scissors +3

    For outcome:
    - Lost +0
    - Tie +3
    - Won +6
     */
    fn fight(&self, opponent_hand: Self) -> i32 {
        match self {
            Rock => match opponent_hand {
                Rock => 1 + 3,
                Paper => 1,
                Scissors => 1 + 6,
            },
            Paper => match opponent_hand {
                Rock => 2 + 6,
                Paper => 2 + 3,
                Scissors => 2,
            },
            Scissors => match opponent_hand {
                Rock => 3,
                Paper => 3 + 6,
                Scissors => 3 + 3,
            },
        }
    }
}

fn solve(rules: Vec<String>) -> i32 {
    let mut score = 0;
    for r in rules {
        if r.len() != 3 {
            panic!("invalid string `{}`", r);
        }
        let opponent_char = r.chars().nth(0).expect("oops");
        let opponent = Hand::from_string(opponent_char);
        let my_char = r.chars().nth(2).expect("oops");
        let myself = Hand::from_string(my_char);
        let this_round_score = myself.fight(opponent);
        score += this_round_score;
    }
    score
}

#[cfg(test)]
mod tests {
    use crate::solve;

    #[test]
    fn ok() {
        let input_raw = vec![
            "A Y",
            "B X",
            "C Z",
        ];
        let input = input_raw.iter().map(|c| String::from(*c)).collect();
        let score = solve(input);
        assert_eq!(score, 15);
    }
}
