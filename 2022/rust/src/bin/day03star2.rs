use std::collections::{HashSet};

mod common;

const DEFAULT_PATH: &str = "./input/input-day03star2.txt";

fn main() {
    let path = common::get_path_from_first_arg(DEFAULT_PATH);
    let content = common::read_file_as_lines(&*path);
    let score = solve(content);
    println!("score {}", score);
}

fn solve(backpacks: Vec<String>) -> i32 {
    let mut sum_of_priorities = 0;
    let mut i = 0;
    while i < backpacks.len() {
        let bag_1 = &backpacks[i];
        let bag_2 = &backpacks[i + 1];
        let bag_3 = &backpacks[i + 2];
        let duplicated = solve_group(bag_1, bag_2, bag_3);
        let priority = calculate_priority(duplicated);
        sum_of_priorities += priority;
        i += 3;
    }
    sum_of_priorities
}

fn solve_group(bag_1: &str, bag_2: &str, bag_3: &str) -> char {
    let bag_1_items: HashSet<char> = HashSet::from_iter(bag_1.chars());
    let bag_2_items: HashSet<char> = HashSet::from_iter(bag_2.chars());
    let bag_3_items: HashSet<char> = HashSet::from_iter(bag_3.chars());
    for c in bag_1_items {
        if bag_2_items.contains(&c) && bag_3_items.contains(&c) {
            return c.clone();
        }
    }
    panic!("beep, not found!")
}

/**
Priority:
- Lowercase item types a through z have priorities 1 through 26.
- Uppercase item types A through Z have priorities 27 through 52.
 */
fn calculate_priority(c: char) -> i32 {
    // ASCII
    // A -> 65
    // a -> 97
    match c {
        'A'..='Z' => (c as i32) - 65 + 27,
        'a'..='z' => (c as i32) - 96,
        _ => panic!("ouch! wrong char `{}`", c),
    }
}

#[cfg(test)]
mod tests {
    use crate::{calculate_priority, solve_group};

    #[test]
    fn priority_test() {
        assert_eq!(calculate_priority('a'), 1);
        assert_eq!(calculate_priority('z'), 26);
        assert_eq!(calculate_priority('A'), 27);
        assert_eq!(calculate_priority('Z'), 52);
    }

    #[test]
    fn test_me2() {
        let c = solve_group(&"aabcd".to_string(), &"defghj".to_string(), &"polkiujd".to_string());
        assert_eq!(c, 'd');
    }
}
