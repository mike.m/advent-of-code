use crate::Hand::{Paper, Rock, Scissors};

mod common;

const DEFAULT_PATH: &str = "./input/input-day02star1.txt";

fn main() {
    let path = common::get_path_from_first_arg(DEFAULT_PATH);
    let content = common::read_file_as_lines(&*path);
    let score = solve(content);
    println!("score {}", score);
}

enum Hand {
    Rock,
    Paper,
    Scissors,
}

impl Hand {
    fn from_string(hand: char) -> Self {
        match hand {
            'A' => Rock,
            'B' => Paper,
            'C' => Scissors,
            _ => panic!("invalid letter `{}`", hand),
        }
    }

    /**
    Returns the score.

    For selected hand:
    - Rock +1
    - Paper +2
    - Scissors +3

    For outcome:
    - Lost +0
    - Tie +3
    - Won +6

    X means you need to lose,
    Y means you need to end the round in a draw,
    and Z means you need to win.
     */
    fn fight(&self, my_hand: char) -> i32 {
        match self {
            Rock => match my_hand {
                'X' => 3,
                'Y' => 3 + 1,
                'Z' => 6 + 2,
                _ => panic!("invalid letter `{}`", my_hand),
            },
            Paper => match my_hand {
                'X' => 1,
                'Y' => 3 + 2,
                'Z' => 6 + 3,
                _ => panic!("invalid letter `{}`", my_hand),
            },
            Scissors => match my_hand {
                'X' => 2,
                'Y' => 3 + 3,
                'Z' => 6 + 1,
                _ => panic!("invalid letter `{}`", my_hand),
            },
        }
    }
}

fn solve(rules: Vec<String>) -> i32 {
    let mut score = 0;
    for r in rules {
        if r.len() != 3 {
            panic!("invalid string `{}`", r);
        }
        let opponent_char = r.chars().nth(0).expect("oops");
        let opponent = Hand::from_string(opponent_char);
        let my_char = r.chars().nth(2).expect("oops");
        let this_round_score = opponent.fight(my_char);
        score += this_round_score;
    }
    score
}

#[cfg(test)]
mod tests {
    use crate::solve;

    #[test]
    fn ok() {
        let input_raw = vec![
            "A Y",
            "B X",
            "C Z",
        ];
        let input = input_raw.iter().map(|c| String::from(*c)).collect();
        let score = solve(input);
        assert_eq!(score, 12);
    }
}
