mod common;

const DEFAULT_PATH: &str = "./input/input-day01star1.txt";

fn main() {
    let path = common::get_path_from_first_arg(DEFAULT_PATH);
    let content = common::read_file_as_lines(&*path);
    let calories = solve(content);
    println!("calories {}", calories);
}

fn solve(calories: Vec<String>) -> i32 {
    let mut temp_calories = 0;
    let mut sorted_calories: Vec<i32> = vec![];
    for cal_as_str in calories {
        if cal_as_str == "" {
            sorted_calories.push(temp_calories);
            temp_calories = 0;
            continue;
        }
        let cal = cal_as_str.parse::<i32>().expect("unlikely to happen");
        temp_calories += cal;
    }
    let last = sorted_calories.last().expect("no way! impossible!");
    if temp_calories > *last {
        sorted_calories.push(temp_calories);
    }
    sorted_calories.sort();
    sorted_calories.iter().skip(sorted_calories.len()-3).sum::<i32>()
}

#[cfg(test)]
mod tests {
    use crate::solve;

    #[test]
    fn ok() {
        let calories = vec!["1", "1", "", "5", "", "2", "1", "", "3", "1000"]
            .iter()
            .map(|c| String::from(*c))
            .collect();
        let result = solve(calories);
        assert_eq!(result, 1011);
    }

    #[test]
    fn ok2() {
        let calories = vec!["1", "1", "", "2", "1", "", "3", "1000", ""]
            .iter()
            .map(|c| String::from(*c))
            .collect();
        let result = solve(calories);
        assert_eq!(result, 1008);
    }

    #[test]
    fn ok3() {
        let calories = vec!["1", "", "2", "", "8", "", "5", "", "10"]
            .iter()
            .map(|c| String::from(*c))
            .collect();
        let result = solve(calories);
        println!("calories ok3 {}", result);
        assert_eq!(result, 23);
    }
}
