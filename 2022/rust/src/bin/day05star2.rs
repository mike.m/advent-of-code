use std::collections::VecDeque;

mod common;

const DEFAULT_PATH: &str = "./input/input-day05star2.txt";

fn main() {
    let path = common::get_path_from_first_arg(DEFAULT_PATH);
    let content = common::read_file_as_lines(&*path);
    let divider_index = find_divider_index(&content);
    let stacks = parse_initial_setup(&content[..divider_index]);
    let top = solve(&content[divider_index + 1..], stacks);
    println!("top {}", top);
}

fn find_divider_index(input: &Vec<String>) -> usize {
    for i in 0..input.len() {
        if input[i].len() == 0 {
            return i;
        }
    }
    panic!("oops, no divider! >_<");
}

fn parse_initial_setup(input: &[String]) -> Vec<VecDeque<char>> {

    // make stacks
    let mut stacks: Vec<VecDeque<char>> = input.last().expect("what? no last?")
        .split(" ")
        .filter(|a| a.len() > 0)
        .map(|_| {
            VecDeque::new() as VecDeque<char>
        })
        .collect();

    // fill stacks
    for line in &input[0..input.len() - 1] {
        let chars: Vec<char> = line.chars().collect();
        for n in 0..stacks.len() {
            let item_index = n * 4 + 1;
            let item = chars.get(item_index).expect(format!("oopsie! no letter at index {}: `{}`", item_index, line).as_str());
            if item != &' ' {
                let deque = &mut stacks[n];
                deque.push_back(*item);
            }
        }
    }
    stacks
}

fn solve(instructions: &[String], mut stacks: Vec<VecDeque<char>>) -> String {
    for i in instructions {
        if i.is_empty() {
            continue;
        }

        let mut split = i.split(' ');
        let amount: usize = split.nth(1).expect("must be amount").parse().expect("amount must be number");

        // indexes in the text starts from 1 but VecDeque is 0-based, so we need to decrease
        let from: usize = split.nth(1).expect("must be from").parse::<usize>().expect("from must be number") - 1;
        let to: usize = split.nth(1).expect("must be to").parse::<usize>().expect("to must be number") - 1;

        if stacks[from].len() < amount {
            panic!("ooops! from stack is too small: need {} got {}", amount, stacks[from].len())
        }
        let mut temp: Vec<char> = Vec::with_capacity(amount);
        for _ in 0..amount {
            let from_stack = &mut stacks[from];
            let removed = from_stack.pop_front().expect("removed not exists");
            temp.push(removed);
        }
        for t in temp {
            let to_stack = &mut stacks[to];
            to_stack.push_front(t);
        }
    }
    stacks.iter().map(|stack| stack[0]).collect()
}

#[cfg(test)]
mod tests {
    use std::collections::VecDeque;

    use crate::{parse_initial_setup, solve};

// TEST INPUT:
    //     [D]
    // [N] [C]
    // [Z] [M] [P]
    //  1   2   3
    //
    // move 1 from 2 to 1
    // move 3 from 1 to 3
    // move 2 from 2 to 1
    // move 1 from 1 to 2

    #[test]
    fn test_setup() {
        let input = vec![
            "    [D]    ".to_string(),
            "[N] [C]    ".to_string(),
            "[Z] [M] [P]".to_string(),
            " 1   2   3 ".to_string(),
        ];
        input.iter().for_each(|line| println!("{}", line));
        let stacks = parse_initial_setup(&input);
        for s in &stacks {
            println!("-----");
            for c in s {
                println!("{}", c);
            }
        }
        let mut expect_stack_1 = VecDeque::new();
        expect_stack_1.push_front('Z');
        expect_stack_1.push_front('N');
        let mut expect_stack_2 = VecDeque::new();
        expect_stack_2.push_front('M');
        expect_stack_2.push_front('C');
        expect_stack_2.push_front('D');
        let expect_stack_3 = VecDeque::from(['P']);
        let expect = vec![expect_stack_1, expect_stack_2, expect_stack_3];
        assert_eq!(stacks, expect);
    }

    #[test]
    fn test_solve() {
        // Each VecDeque represents one stack. Beginning (index 0) is the top of the stack.
        let stacks = vec![
            VecDeque::from(['N', 'Z']),
            VecDeque::from(['D', 'C', 'M']),
            VecDeque::from(['P']),
        ];
        let input = vec![
            "move 1 from 2 to 1".to_string(),
            "move 3 from 1 to 3".to_string(),
            "move 2 from 2 to 1".to_string(),
            "move 1 from 1 to 2".to_string(),
        ];
        let top = solve(&input, stacks);
        assert_eq!(top, "CMZ");
    }
}
