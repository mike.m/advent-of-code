mod common;

const DEFAULT_PATH: &str = "./input/input-day01star2.txt";

fn main() {
    let path = common::get_path_from_first_arg(DEFAULT_PATH);
    let content = common::read_file_as_lines(&*path);
    let calories = solve(content);
    println!("calories {}", calories);
}

fn solve(calories: Vec<String>) -> i32 {
    let mut temp_calories = 0;
    let mut max_calories = 0;
    for cal_as_str in calories {
        if cal_as_str == "" {
            if temp_calories > max_calories {
                max_calories = temp_calories;
            }
            temp_calories = 0;
            continue;
        }
        let cal = cal_as_str.parse::<i32>().expect("unlikely to happen");
        temp_calories += cal;
    }
    if temp_calories > max_calories {
        max_calories = temp_calories;
    }
    max_calories
}

#[cfg(test)]
mod tests {
    use crate::solve;

    #[test]
    fn ok() {
        let calories = vec!["1", "1", "", "2", "1", "", "3", "1000"]
            .iter()
            .map(|c| String::from(*c))
            .collect();
        let result = solve(calories);
        assert_eq!(result, 1003);
    }

    #[test]
    fn ok2() {
        let calories = vec!["1", "1", "", "2", "1", "", "3", "1000", ""]
            .iter()
            .map(|c| String::from(*c))
            .collect();
        let result = solve(calories);
        assert_eq!(result, 1003);
    }

    #[test]
    fn ok3() {
        let calories = vec!["1", "1", "", "2", "1", "", "3", "1000", "", "", "", ""]
            .iter()
            .map(|c| String::from(*c))
            .collect();
        let result = solve(calories);
        assert_eq!(result, 1003);
    }
}
