use std::collections::{HashSet};

mod common;

const DEFAULT_PATH: &str = "./input/input-day03star1.txt";

fn main() {
    let path = common::get_path_from_first_arg(DEFAULT_PATH);
    let content = common::read_file_as_lines(&*path);
    let score = solve(content);
    println!("score {}", score);
}

fn solve(backpacks: Vec<String>) -> i32 {
    let mut sum_of_priorities = 0;
    for b in backpacks {
        let pocket1 = &b[..b.len() / 2];
        let pocket2 = &b[b.len() / 2..];
        let duplicated = duplicated_item(pocket1, pocket2);
        let priority = calculate_priority(duplicated);
        sum_of_priorities += priority;
    }
    sum_of_priorities
}

fn duplicated_item(pocket_1: &str, pocket_2: &str) -> char {
    let pocket_1_items:HashSet<char> = HashSet::from_iter(pocket_1.chars());
    let pocket_2_items:HashSet<char> = HashSet::from_iter(pocket_2.chars());

    let &count = &pocket_1_items.iter().filter(|c| pocket_2_items.contains(c)).count();
    if count > 1 {
        // Just in case as I'm not sure I understand the problem correctly.
        panic!("whoa! count={}", count);
    }

    let found_maybe = &pocket_1_items.iter().find(|c| pocket_2_items.contains(c));
    match found_maybe {
        Some(c) => **c,
        None => panic!("ouch, not found, but that shouldn't happen"),
    }
}

/**
Priority:
- Lowercase item types a through z have priorities 1 through 26.
- Uppercase item types A through Z have priorities 27 through 52.
 */
fn calculate_priority(c: char) -> i32 {
    // ASCII
    // A -> 65
    // a -> 97
    match c {
        'A'..='Z' => (c as i32) - 65 + 27,
        'a'..='z' => (c as i32) - 96,
        _ => panic!("ouch! wrong char `{}`", c),
    }
}

#[cfg(test)]
mod tests {
    use crate::{calculate_priority, duplicated_item};

    #[test]
    fn priority_test() {
        assert_eq!(calculate_priority('a'), 1);
        assert_eq!(calculate_priority('z'), 26);
        assert_eq!(calculate_priority('A'), 27);
        assert_eq!(calculate_priority('Z'), 52);
    }

    #[test]
    fn test_me2() {
        let c = duplicated_item("ZTmtZvZLTFNLMQMNRvZ", "ncdcHwcScJvcdHnVfwV");
        println!("{}", c)
    }
}
