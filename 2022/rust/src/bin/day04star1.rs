use std::collections::{HashSet};

mod common;

const DEFAULT_PATH: &str = "./input/input-day04star1.txt";

fn main() {
    let path = common::get_path_from_first_arg(DEFAULT_PATH);
    let content = common::read_file_as_lines(&*path);
    let score = solve(content);
    println!("score {}", score);
}

fn solve(cleaning_pair: Vec<String>) -> i32 {
    let mut count_of_overlaps = 0;
    for raw_task_details in cleaning_pair {
        let (a_start, a_end, b_start, b_end) = parse_task(&raw_task_details);
        if (b_start >= a_start && b_end <= a_end) || (a_start >= b_start && a_end <= b_end) {
            count_of_overlaps += 1;
        }
    }
    count_of_overlaps
}

fn parse_task(task_details: &str) -> (i32, i32, i32, i32) {
    let s: Vec<&str> = task_details.split([',', '-']).collect();
    if s.len() != 4 {
        panic!("ouch, that shouldn't happen! `{}`", task_details);
    }
    (
        s[0].parse().expect(&format!("err 0 `{}`", task_details)),
        s[1].parse().expect(&format!("err 1 `{}`", task_details)),
        s[2].parse().expect(&format!("err 2 `{}`", task_details)),
        s[3].parse().expect(&format!("err 3 `{}`", task_details)),
    )
}

#[cfg(test)]
mod tests {
    use crate::{parse_task, solve};

    #[test]
    fn test_task_parsing() {
        let (a_start, a_end, b_start, b_end) = parse_task("2-6,4-8");
        assert_eq!(a_start, 2);
        assert_eq!(a_end, 6);
        assert_eq!(b_start, 4);
        assert_eq!(b_end, 8);
    }

    #[test]
    fn test_overlaps_counting() {
        let input = vec![
            "2-4,6-8".to_string(),
            "2-3,4-5".to_string(),
            "5-7,7-9".to_string(),
            "2-8,3-7".to_string(),
            "6-6,4-6".to_string(),
            "2-6,4-8".to_string(),
        ];
        let count = solve(input);
        assert_eq!(count, 2);
    }
}
