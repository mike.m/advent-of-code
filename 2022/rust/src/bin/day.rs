use std::fmt::{Display, Formatter};
use std::thread::sleep;
use std::time;

struct CustomString {
    content: String,
}

impl Display for CustomString {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "`{}`", self.content)
    }
}

impl Drop for CustomString {
    fn drop(&mut self) {
        println!("ouch, i've been: {}", self);
    }
}

fn main() {
    let cs2 = CustomString { content: "haha2".to_string() };
    {
        let cs = CustomString { content: "haha".to_string() };
        println!("i am: {}", cs);
    }
    println!("i am 2: {}", cs2);
}
