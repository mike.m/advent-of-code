mod common;

const DEFAULT_PATH: &str = "./input/input-day02.txt";

const MAX_RED: u32 = 12;
const MAX_GREEN: u32 = 13;
const MAX_BLUE: u32 = 14;

fn main() {
    let path = common::get_path_from_first_arg(DEFAULT_PATH);
    let content = common::read_file_as_lines(&*path);
    let input = content.iter().map(|s| s.as_str()).collect();
    let sum = solve(input);
    println!("value {}", sum);
}

fn solve(content: Vec<&str>) -> u32 {
    content
        .iter()
        .map(|s| solve_one_game(*s))
        .filter(|id| id.is_some())
        .map(|n| n.unwrap())
        .sum()
}

fn solve_one_game(s: &str) -> Option<u32> {
    let colon_index = s.find(":").unwrap();
    let game_id_chunk = &s[..colon_index];
    let cube_draws_chunk = &s[colon_index + 2..];
    let game_id = extract_game_id(game_id_chunk);
    let normalized_draws = cube_draws_chunk.replace("; ", ", ");
    let all_games_good = normalized_draws.split(", ")
        .map(|draw_cubes| is_draw_possible(draw_cubes))
        .all(|result| result);
    return match all_games_good {
        true => Some(game_id),
        false => None,
    };
}

fn is_draw_possible(s: &str) -> bool {
    let chunks: Vec<&str> = s.split(" ").collect();
    let n = chunks[0].parse::<u32>().unwrap();
    match chunks[1] {
        "red" => n <= MAX_RED,
        "green" => n <= MAX_GREEN,
        "blue" => n <= MAX_BLUE,
        &_ => panic!("invalid color `{}`", chunks[1])
    }
}

/// Extract game id from string. The format must be "Game N" where N is a number, which may have any number of digits.
fn extract_game_id(s: &str) -> u32 {
    let n = &s[5..];
    n.parse::<u32>().unwrap()
}

/*
 Game 5: 15 red, 3 green, 1 blue; 6 red, 2 blue, 2 green; 3 green, 3 red, 1 blue; 2 blue, 13 red, 5 green; 2 green, 15 red, 2 blue

 - find : and split in half
 - parse game id
 - split games with semicolon
 - split each game with comma
 - asses colors and numbers
 */

#[cfg(test)]
mod tests {
    // use crate::{solve};

    use crate::{extract_game_id, is_draw_possible, solve, solve_one_game};

    #[test]
    fn game_id_ok() {
        let test_cases = vec![("Game 1", 1), ("Game 2", 2), ("Game 101", 101)];
        for tc in test_cases {
            let (input, expected) = tc;
            assert_eq!(extract_game_id(input), expected);
        }
    }

    #[test]
    fn is_draw_possible_ok() {
        let test_cases = vec![
            ("1 red", true),
            ("11 red", true),
            ("12 red", true),
            ("13 red", false),
            ("99 red", false),
            ("1 green", true),
            ("12 green", true),
            ("13 green", true),
            ("14 green", false),
            ("99 green", false),
            ("1 blue", true),
            ("13 blue", true),
            ("14 blue", true),
            ("15 blue", false),
            ("99 blue", false),
        ];
        for tc in test_cases {
            let (input, expected) = tc;
            assert_eq!(is_draw_possible(input), expected);
        }
    }

    #[test]
    fn solve_one_game_ok() {
        let good_game = "Game 3: 1 red, 9 green, 3 blue; 8 green, 4 red, 11 blue; 6 red, 10 blue; 6 green, 6 red, 12 blue; 2 blue, 11 green, 7 red; 12 blue, 9 green, 8 red";
        assert_eq!(solve_one_game(good_game), Some(3));
        let good_game = "Game 3: 1 red, 9 green, 3 blue; 8 green, 4 red, 11 blue; 6 red, 10 blue; 6 green, 6 red, 12 blue; 2 blue, 110 green, 7 red; 12 blue, 9 green, 8 red";
        assert_eq!(solve_one_game(good_game), None);
    }

    #[test]
    fn solve_ok() {
        let result = solve(vec![
            "Game 1: 1 red, 9 green, 3 blue",
            "Game 2: 111 red, 9 green, 3 blue",
            "Game 3: 1 red, 9 green, 3 blue",
            "Game 4: 1 red, 9 green, 3333 blue",
            "Game 5: 1 red, 9 green, 3 blue",
        ]);
        assert_eq!(result, 9)
    }
}
