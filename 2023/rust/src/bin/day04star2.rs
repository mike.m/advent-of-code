use std::collections::{HashMap, HashSet};

mod common;

const DEFAULT_PATH: &str = "./input/input-day04.txt";

fn main() {
    let path = common::get_path_from_first_arg(DEFAULT_PATH);
    let content = common::read_file_as_lines(&*path);
    let input = content.iter().map(|s| s.as_str()).collect();
    let sum = solve(input);
    println!("solution = {}", sum);
}

fn solve(content: Vec<&str>) -> u32 {
    let mut cards = prepare_cards(content);
    add_copy_of_winning_cards(&mut cards);
    let mut sum = 0;
    for c in cards {
        sum += c.1.count
    }
    sum
}

#[derive(Debug, PartialOrd, PartialEq)]
struct Card {
    count: u32,
    value: u32,
}

fn prepare_cards(content: Vec<&str>) -> HashMap<u32, Card> {
    content
        .iter()
        .map(|s| {
            let index_colon = s.find(":").unwrap();
            let card_id: u32 = s[5..index_colon].trim_start().parse().unwrap();
            let numbers = &s[index_colon + 1..];
            let value = card_value(numbers);
            (card_id, Card { count: 1, value })
        })
        .collect()
}

fn add_copy_of_winning_cards(cards: &mut HashMap<u32, Card>) {
    let mut i = 1;
    loop {
        match cards.get(&i) {
            None => break,
            Some(card) => {
                let c = card.count;
                let v = card.value;

                // this happens as many times as the winning number
                for j in 0..v {
                    let subsequent_index = i + j + 1;
                    let subsequent_card = cards
                        .get(&(subsequent_index))
                        .expect(format!("card number {} not found", subsequent_index).as_str());
                    let card_added = Card { count: subsequent_card.count + c, value: subsequent_card.value };
                    cards.insert(subsequent_index, card_added);
                }
            }
        }
        i += 1;
    }
}

fn card_value(s: &str) -> u32 {
    let index_pipe = s.find("|").unwrap();
    let win_numbers = extract_numbers(&s[..index_pipe]);
    let card_numbers = extract_numbers(&s[index_pipe + 1..]);
    let win_count = card_numbers
        .iter()
        .filter(|n| win_numbers.contains(n))
        .count() as u32;
    win_count
}

fn extract_numbers(card_numbers_part: &str) -> HashSet<u32> {
    card_numbers_part
        .split(" ")
        .filter(|s| !s.is_empty())
        .map(|s| s.parse().unwrap())
        .collect()
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use crate::{add_copy_of_winning_cards, Card, prepare_cards, solve};

    #[test]
    fn test_prepare_cards() {
        let input = vec![
            "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53",
            "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19",
            "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1",
            "Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83",
            "Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36",
            "Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11",
        ];
        let cards = prepare_cards(input);
        let expected: HashMap<u32, Card> = HashMap::from([
            (1, Card { count: 1, value: 4 }),
            (2, Card { count: 1, value: 2 }),
            (3, Card { count: 1, value: 2 }),
            (4, Card { count: 1, value: 1 }),
            (5, Card { count: 1, value: 0 }),
            (6, Card { count: 1, value: 0 }),
        ]);
        assert_cards(&cards, &expected);
    }

    fn assert_cards(left: &HashMap<u32, Card>, right: &HashMap<u32, Card>) {
        let left_keys: Vec<&u32> = left.keys().collect();
        let right_keys: Vec<&u32> = left.keys().collect();
        assert_eq!(left_keys, right_keys, "cards ids do not match");
        for i in left_keys {
            let left_card = left.get(&i).unwrap();
            let right_card = right.get(&i).unwrap();
            assert_eq!(left_card, right_card, "card id={} do not match", i)
        }
    }

    #[test]
    fn test_add_copy_of_winning_cards() {
        let mut cards: HashMap<u32, Card> = HashMap::from([
            (1, Card { count: 1, value: 4 }),
            (2, Card { count: 1, value: 2 }),
            (3, Card { count: 1, value: 2 }),
            (4, Card { count: 1, value: 1 }),
            (5, Card { count: 1, value: 0 }),
            (6, Card { count: 1, value: 0 }),
        ]);
        add_copy_of_winning_cards(&mut cards);

        let expect_added_cards: HashMap<u32, Card> = HashMap::from([
            (1, Card { count: 1, value: 4 }),
            (2, Card { count: 2, value: 2 }),
            (3, Card { count: 4, value: 2 }),
            (4, Card { count: 8, value: 1 }),
            (5, Card { count: 14, value: 0 }),
            (6, Card { count: 1, value: 0 }),
        ]);
        assert_cards(&cards, &expect_added_cards);
    }

    #[test]
    fn test_solve() {
        let input = vec![
            "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53",
            "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19",
            "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1",
            "Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83",
            "Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36",
            "Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11",
        ];
        let won_points = solve(input);
        assert_eq!(won_points, 30);
    }
}
