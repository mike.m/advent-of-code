use std::collections::{HashMap, HashSet};

mod common;

const DEFAULT_PATH: &str = "./input/input-day03.txt";

fn main() {
    let path = common::get_path_from_first_arg(DEFAULT_PATH);
    let content = common::read_file_as_lines(&*path);
    let input = content.iter().map(|s| s.as_str()).collect();
    let sum = solve(input);
    println!("solution = {}", sum);
}

fn solve(content: Vec<&str>) -> i32 {
    let gears = get_gear_symbols(&content);
    let mut ids: HashMap<(i32, i32), i32> = HashMap::new();
    let mut numbers: HashMap<i32, &str> = HashMap::new();
    for y in 0..content.len() {
        let row = content[y];
        extract_numbers(row, y, &mut ids, &mut numbers)
    }
    gears
        .iter()
        .map(|(x, y)| calc_gear_power(*x, *y, &ids, &numbers))
        .map(|opt| opt.unwrap_or(0))
        .sum()
}

fn extract_numbers<'a, 'b>(row: &'a str, y: usize, ids: &'b mut HashMap<(i32, i32), i32>, numbers: &'b mut HashMap<i32, &'a str>) {
    let mut start: Option<usize> = None;
    let mut id: i32 = 0;
    for x in 0..row.len() {
        let character = &row[x..x + 1];
        let is_digit = is_digit(character);
        if is_digit {
            if start.is_none() {
                start = Some(x);
                id = y as i32 * 10000 + x as i32;
            }
            ids.insert((x as i32, y as i32), id);
        } else if !is_digit {
            if let Some(start_index) = start {
                let number = &row[start_index..x];
                numbers.insert(id, number);
                start = None;
            }
        }
    }
    if let Some(start_index) = start {
        let number = &row[start_index..row.len()];
        numbers.insert(id, number);
    }
}

fn is_digit(character: &str) -> bool {
    character >= "0" && character <= "9"
}

fn calc_gear_power(x: i32, y: i32, ids: &HashMap<(i32, i32), i32>, number: &HashMap<i32, &str>) -> Option<i32> {
    let coords_around = vec![
        (-1, -1),
        (-1, 0),
        (-1, 1),
        (0, -1),
        (0, 1),
        (1, -1),
        (1, 0),
        (1, 1),
    ];
    let touching_ids: HashSet<i32> = coords_around
        .iter()
        .map(|(delta_x, delta_y)| (x + delta_x, y + delta_y))
        .map(|coords| ids.get(&coords))
        .filter(|opt| opt.is_some())
        .map(|opt| *opt.unwrap())
        .collect();
    if touching_ids.len() == 2 {
        let serial_numbers: Vec<i32> = touching_ids
            .iter()
            .map(|id| *number.get(id).unwrap())
            .map(|number_str| number_str.parse::<i32>().unwrap())
            .collect();
        Some(serial_numbers[0] * serial_numbers[1])
    } else {
        None
    }
}

fn get_gear_symbols(content: &Vec<&str>) -> HashSet<(i32, i32)> {
    let mut symbols: HashSet<(i32, i32)> = HashSet::new();
    for y in 0..content.len() {
        let row = content[y];
        for x in 0..row.len() {
            let character = &row[x..x + 1];
            if character == "*" {
                symbols.insert((x as i32, y as i32));
            }
        }
    }
    symbols
}

#[cfg(test)]
mod tests {
    use std::collections::{HashMap, HashSet};

    use crate::{extract_numbers, get_gear_symbols, solve};

    #[test]
    fn test_get_gear_symbols() {
        let content = vec![
            "!*.1..2..3..4..5...",
            "67890.=..+.*......!",
            "=......---........*",
        ];
        let expected = HashSet::from([(1, 0), (11, 1), (18, 2)]);
        assert_eq!(get_gear_symbols(&content), expected);
    }

    #[test]
    fn test_digit_adjacent_to_symbol() {}

    #[test]
    fn test_extract_serial_number() {}

    #[test]
    fn test_extract_numbers() {
        struct TestCase {
            row: &'static str,
            y: usize,
            expected_ids: HashMap<(i32, i32), i32>,
            expected_numbers: HashMap<i32, &'static str>,
        }

        let test_cases = vec![
            TestCase {
                row: ".1..10...100",
                y: 3,
                expected_ids: HashMap::from([
                    ((1, 3), 30001),
                    ((4, 3), 30004),
                    ((5, 3), 30004),
                    ((9, 3), 30009),
                    ((10, 3), 30009),
                    ((11, 3), 30009),
                ]),
                expected_numbers: HashMap::from([
                    (30001, "1"),
                    (30004, "10"),
                    (30009, "100"),
                ]),
            },
            TestCase {
                row: "1..10...100...",
                y: 99,
                expected_ids: HashMap::from([
                    ((0, 99), 990000),
                    ((3, 99), 990003),
                    ((4, 99), 990003),
                    ((8, 99), 990008),
                    ((9, 99), 990008),
                    ((10, 99), 990008),
                ]),
                expected_numbers: HashMap::from([
                    (990000, "1"),
                    (990003, "10"),
                    (990008, "100"),
                ]),
            },
        ];

        for tc in test_cases {
            let mut ids: HashMap<(i32, i32), i32> = HashMap::new();
            let mut numbers: HashMap<i32, &str> = HashMap::new();
            extract_numbers(tc.row, tc.y, &mut ids, &mut numbers);
            assert_eq!(tc.expected_ids, ids, "ids test failed for row {}", tc.row);
            assert_eq!(tc.expected_numbers, numbers, "numbers test failed for row {}", tc.row);
        }
    }

    #[test]
    fn solve_ok() {
        let content = vec![
            "467..114..",
            "...*......",
            "..35..633.",
            "......#...",
            "617*......",
            ".....+.58.",
            "..592.....",
            "......755.",
            "...$.*....",
            ".664.598..",
        ];
        assert_eq!(solve(content), 467835);
    }
}
