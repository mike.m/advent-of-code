mod common;

const DEFAULT_PATH: &str = "./input/input-day01.txt";

fn main() {
    let path = common::get_path_from_first_arg(DEFAULT_PATH);
    let content = common::read_file_as_lines(&*path);
    let input = content.iter().map(|s| s.as_str()).collect();
    let v = solve(input);
    println!("value {}", v);
}

fn solve(content: Vec<&str>) -> i32 {
    content.iter().map(|i| extract_value(*i)).sum()
}

fn extract_value(s: &str) -> i32 {
    let left_digit = s.chars()
        .filter(|c| *c >= '0' && *c <= '9')
        .nth(0)
        .map(|c| c.to_digit(10).unwrap())
        .expect(format!("left digit failed for input {}", s).as_str());

    let right_digit = s.chars()
        .filter(|c| *c >= '0' && *c <= '9')
        .nth_back(0)
        .map(|c| c.to_digit(10).unwrap())
        .expect(format!("right digit failed for input {}", s).as_str());

    (left_digit * 10 + right_digit) as i32
}

#[cfg(test)]
mod tests {
    use crate::{extract_value, solve};

    #[test]
    fn extract_ok() {
        let test_cases = vec![
            ("aaa12aas", 12),
            ("aaa4aas", 44),
            ("3aaa12aas5", 35),
            ("66", 66),
            ("8", 88),
            ("b90d", 90),
            ("9zzz", 99),
        ];
        for tc in test_cases {
            let (input, expected) = tc;
            let value = extract_value(input);
            assert_eq!(value, expected, "failed for input {}", input)
        }
    }

    #[test]
    fn solve_ok() {
        let input = vec!["12", "b90d", "5", "a3a4a5a6a7"];
        let sum = solve(input);
        assert_eq!(sum, 12 + 90 + 55 + 37)
    }
}
