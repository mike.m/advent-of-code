use std::collections::HashSet;

mod common;

const DEFAULT_PATH: &str = "./input/input-day04.txt";

fn main() {
    let path = common::get_path_from_first_arg(DEFAULT_PATH);
    let content = common::read_file_as_lines(&*path);
    let input = content.iter().map(|s| s.as_str()).collect();
    let sum = solve(input);
    println!("solution = {}", sum);
}

fn solve(content: Vec<&str>) -> u32 {
    content
        .iter()
        .map(|s| card_value(s))
        .sum()
}

fn card_value(s: &str) -> u32 {
    let index_colon = s.find(":").unwrap();
    let index_pipe = s.find("|").unwrap();
    let win_numbers = extract_numbers(&s[index_colon + 1..index_pipe]);
    let card_numbers = extract_numbers(&s[index_pipe + 1..]);
    let win_count = card_numbers
        .iter()
        .filter(|n| win_numbers.contains(n))
        .count() as u32;
    if win_count == 0 {
        0
    } else {
        2u32.pow(win_count - 1)
    }
}

fn extract_numbers(card_numbers_part: &str) -> HashSet<u32> {
    card_numbers_part
        .split(" ")
        .filter(|s| !s.is_empty())
        .map(|s| s.parse().unwrap())
        .collect()
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    use crate::{card_value, extract_numbers};

    #[test]
    fn test_extract_numbers() {
        let test_cases = vec![
            ("1 2 3 4", HashSet::from([1, 2, 3, 4])),
            ("     199", HashSet::from([199])),
            ("199 15     31           ", HashSet::from([199, 15, 31])),
        ];
        for tc in test_cases {
            let (input, expect_numbers) = tc;
            assert_eq!(extract_numbers(input), expect_numbers, "failed for input {}", input);
        }
    }

    #[test]
    fn test_card_value() {
        let test_cases = vec![
            ("Game1:1 2 3 4|5 6 7 8 9 10", 0),
            ("Game2:1 2 3 4|5 6 7 1 9 10", 1),
            ("Game3:1 2 3 4|5 6 7 1 9 2", 2),
            ("Game4:1 2 3 4|5 3 7 1 9 2", 4),
            ("Game5 : 1 2 3 4 | 5 3 7 1 4 2", 8),
        ];
        for tc in test_cases {
            let (input, expect_numbers) = tc;
            assert_eq!(card_value(input), expect_numbers, "failed for input {}", input);
        }
    }
}
