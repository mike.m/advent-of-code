use std::{env, fs, io};
use std::io::BufRead;

pub fn read_file_as_lines(path: &str) -> Vec<String> {
    let f = fs::File::open(path).expect(format!("failed: {}", path).as_str());
    let lines = io::BufReader::new(f).lines();
    lines.map(|l| l.expect("Could not parse line")).collect()
}

pub fn get_path_from_first_arg(default_path: &str) -> String {
    let option = env::args().nth(1);
    if option.is_none() {
        default_path.to_string()
    } else {
        option.expect("ain't gonna happen")
    }
}
