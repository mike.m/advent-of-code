mod common;

const DEFAULT_PATH: &str = "./input/input-day02.txt";

fn main() {
    let path = common::get_path_from_first_arg(DEFAULT_PATH);
    let content = common::read_file_as_lines(&*path);
    let input = content.iter().map(|s| s.as_str()).collect();
    let sum = solve(input);
    println!("value {}", sum);
}

fn solve(content: Vec<&str>) -> u32 {
    content
        .iter()
        .map(|s| solve_one_game(*s))
        .sum()
}

fn solve_one_game(s: &str) -> u32 {
    let colon_index = s.find(":").unwrap();
    let game_id_chunk = &s[..colon_index];
    let cube_draws_chunk = &s[colon_index + 2..];
    extract_game_id(game_id_chunk);
    let normalized_draws = cube_draws_chunk.replace("; ", ", ");

    let mut min_red = 0;
    let mut min_green = 0;
    let mut min_blue = 0;

    normalized_draws.split(", ")
        .map(|draw_cubes| parse_drawn_cubes(draw_cubes))
        .for_each(|(n, color)| {
            match color {
                "red" => min_red = if n > min_red { n } else { min_red },
                "green" => min_green = if n > min_green { n } else { min_green },
                "blue" => min_blue = if n > min_blue { n } else { min_blue },
                &_ => panic!("wrong color `{}`", color)
            }
        });
    min_red * min_green * min_blue
}

/// Extract game id from string. The format must be "Game N" where N is a number, which may have any number of digits.
fn extract_game_id(s: &str) -> u32 {
    let n = &s[5..];
    n.parse::<u32>().unwrap()
}

fn parse_drawn_cubes(s: &str) -> (u32, &str) {
    let chunks: Vec<&str> = s.split(" ").collect();
    let n = chunks[0].parse::<u32>().unwrap();
    (n, chunks[1])
}

#[cfg(test)]
mod tests {
    // use crate::{solve};

    use crate::{extract_game_id, parse_drawn_cubes, solve, solve_one_game};

    #[test]
    fn game_id_ok() {
        let test_cases = vec![("Game 1", 1), ("Game 2", 2), ("Game 101", 101)];
        for tc in test_cases {
            let (input, expected) = tc;
            assert_eq!(extract_game_id(input), expected);
        }
    }

    #[test]
    fn parse_drawn_cubes_ok() {
        let test_cases = vec![
            ("1 red", (1, "red")),
            ("11 red", (11, "red")),
            ("12 red", (12, "red")),
            ("13 red", (13, "red")),
            ("99 red", (99, "red")),
            ("1 green", (1, "green")),
            ("12 green", (12, "green")),
            ("13 green", (13, "green")),
            ("14 green", (14, "green")),
            ("99 green", (99, "green")),
            ("1 blue", (1, "blue")),
            ("13 blue", (13, "blue")),
            ("14 blue", (14, "blue")),
            ("15 blue", (15, "blue")),
            ("99 blue", (99, "blue")),
        ];
        for tc in test_cases {
            let (input, expected) = tc;
            assert_eq!(parse_drawn_cubes(input), expected, "failed for input {}", input);
        }
    }

    #[test]
    fn solve_one_game_ok() {
        let test_cases = vec![
            ("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green", 48),
            ("Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue", 12),
            ("Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red", 1560),
            ("Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red", 630),
            ("Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green", 36),
        ];
        for tc in test_cases {
            let (input, expected) = tc;
            assert_eq!(solve_one_game(input), expected, "failed for input: {}", input);
        }
    }

    #[test]
    fn solve_ok() {
        let result = solve(vec![
            "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green",
            "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue",
            "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red",
            "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red",
            "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green",
        ]);
        assert_eq!(result, 2286)
    }
}
