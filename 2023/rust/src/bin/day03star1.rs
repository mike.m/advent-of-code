use std::collections::HashSet;

mod common;

const DEFAULT_PATH: &str = "./input/input-day03.txt";

fn main() {
    let path = common::get_path_from_first_arg(DEFAULT_PATH);
    let content = common::read_file_as_lines(&*path);
    let input = content.iter().map(|s| s.as_str()).collect();
    let sum = solve(input);
    println!("solution = {}", sum);
}

fn solve(content: Vec<&str>) -> u32 {
    let mut sum = 0;
    let symbols = get_symbols(&content);
    for y in 0..content.len() {
        let row = content[y];
        extract_numbers(row, y, &symbols)
            .iter()
            .for_each(|n| sum = sum + n);
    }
    sum
}

fn extract_numbers(row: &str, y: usize, symbols: &HashSet<(i32, i32)>) -> Vec<u32> {
    let mut start: Option<usize> = None;
    let mut numbers: Vec<u32> = Vec::new();
    for x in 0..row.len() {
        let character = &row[x..x + 1];
        let is_digit = is_digit(character);
        if is_digit && start.is_none() {
            start = Some(x);
        } else if !is_digit {
            if let Some(start_index) = start {
                let sn = extract_serial_number(&row[start_index..x], &symbols, start_index, y);
                if let Some(n) = sn { numbers.push(n) };
                start = None;
            }
        }
    }

    if let Some(start_index) = start {
        let sn = extract_serial_number(&row[start_index..], &symbols, start_index, y);
        if let Some(n) = sn { numbers.push(n) };
    }

    numbers
}

fn is_digit(character: &str) -> bool {
    character >= "0" && character <= "9"
}

fn extract_serial_number(s: &str, symbols: &HashSet<(i32, i32)>, x: usize, y: usize) -> Option<u32> {
    let touches = (0..s.len())
        .map(|i| digit_adjacent_to_symbol((x + i) as i32, y as i32, symbols))
        .any(|touches_symbol| touches_symbol);
    if touches {
        Some(s.parse().unwrap())
    } else {
        None
    }
}

fn digit_adjacent_to_symbol(x: i32, y: i32, symbols: &HashSet<(i32, i32)>) -> bool {
    let coords = vec![
        (-1, -1),
        (-1, 0),
        (-1, 1),
        (0, -1),
        (0, 1),
        (1, -1),
        (1, 0),
        (1, 1),
    ];
    return coords
        .iter()
        .map(|(a, b)| (*a + x, *b + y))
        .map(|coords| symbols.contains(&coords))
        .any(|found| found);
}

fn get_symbols(content: &Vec<&str>) -> HashSet<(i32, i32)> {
    let mut symbols: HashSet<(i32, i32)> = HashSet::new();
    for y in 0..content.len() {
        let row = content[y];
        for x in 0..row.len() {
            let character = &row[x..x + 1];
            if (character < "0" || character > "9") && character != "." {
                symbols.insert((x as i32, y as i32));
            }
        }
    }
    symbols
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    use crate::{digit_adjacent_to_symbol, extract_numbers, extract_serial_number, get_symbols, solve};

    #[test]
    fn test_get_symbols() {
        let content = vec![
            ".$.1..2..3..4..5...",
            "67890....+........!",
            "=..................",
        ];
        let expected = HashSet::from([(1, 0), (9, 1), (18, 1), (0, 2)]);
        assert_eq!(get_symbols(&content), expected);
    }

    #[test]
    fn test_digit_adjacent_to_symbol() {
        #[derive(Debug)]
        struct TestCase {
            x: i32,
            y: i32,
            expected: bool,
        }
        let symbols = HashSet::from([(0, 0), (9, 1), (18, 1), (0, 2)]);
        let test_cases = vec![
            TestCase { x: 0, y: 1, expected: true },
            TestCase { x: 0, y: 2, expected: false },
            TestCase { x: 1, y: 0, expected: true },
            TestCase { x: 1, y: 1, expected: true },
            TestCase { x: 8, y: 0, expected: true },
            TestCase { x: 8, y: 1, expected: true },
            TestCase { x: 8, y: 2, expected: true },
            TestCase { x: 9, y: 0, expected: true },
            TestCase { x: 9, y: 2, expected: true },
            TestCase { x: 10, y: 0, expected: true },
            TestCase { x: 10, y: 1, expected: true },
            TestCase { x: 10, y: 2, expected: true },
            TestCase { x: 17, y: 0, expected: true },
            TestCase { x: 17, y: 1, expected: true },
            TestCase { x: 17, y: 2, expected: true },
            TestCase { x: 18, y: 0, expected: true },
            TestCase { x: 18, y: 2, expected: true },
            TestCase { x: 19, y: 0, expected: true },
            TestCase { x: 19, y: 1, expected: true },
            TestCase { x: 19, y: 2, expected: true },
            TestCase { x: 0, y: 1, expected: true },
            TestCase { x: 0, y: 3, expected: true },
            TestCase { x: 1, y: 1, expected: true },
            TestCase { x: 1, y: 2, expected: true },
            TestCase { x: 0, y: 1, expected: true },
            TestCase { x: 5, y: 5, expected: false },
        ];
        for tc in test_cases {
            assert_eq!(digit_adjacent_to_symbol(tc.x, tc.y, &symbols), tc.expected, "failed for {:?}", tc);
        }
    }

    #[test]
    fn test_extract_serial_number() {
        let symbols = HashSet::from([(3, 3)]);
        assert_eq!(extract_serial_number("123", &symbols, 4, 4), Some(123));
        assert_eq!(extract_serial_number("456", &symbols, 0, 3), Some(456));
        assert_eq!(extract_serial_number("111", &symbols, 5, 3), None);
    }

    #[test]
    fn test_extract_numbers() {
        struct TestCase {
            row: &'static str,
            y: usize,
            symbols: HashSet<(i32, i32)>,
            expected_numbers: Vec<u32>,
        }

        let test_cases = vec![
            TestCase {
                row: ".1..10...100",
                y: 3,
                symbols: HashSet::from([(0, 3), (10, 4)]),
                expected_numbers: vec![1, 100],
            },
            TestCase {
                row: "1..10...100...",
                y: 3,
                symbols: HashSet::from([(0, 2), (3, 4)]),
                expected_numbers: vec![1, 10],
            },
        ];

        for tc in test_cases {
            let got = extract_numbers(tc.row, tc.y, &tc.symbols);
            assert_eq!(got, tc.expected_numbers);
        }
    }

    #[test]
    fn solve_ok() {
        let content = vec![
            "467..114..",
            "...*......",
            "..35..633.",
            "......#...",
            "617*......",
            ".....+.58.",
            "..592.....",
            "......755.",
            "...$.*....",
            ".664.598..",
        ];
        assert_eq!(solve(content), 4361);
    }
}
