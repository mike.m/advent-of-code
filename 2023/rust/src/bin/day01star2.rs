mod common;

const DEFAULT_PATH: &str = "./input/input-day01.txt";

fn main() {
    let path = common::get_path_from_first_arg(DEFAULT_PATH);
    let content = common::read_file_as_lines(&*path);
    let input = content.iter().map(|s| s.as_str()).collect();
    let v = solve(input);
    println!("value {}", v);
}

fn solve(content: Vec<&str>) -> u32 {
    content
        .iter()
        .map(|&i| extract_left_value(i) * 10 + extract_right_value(i))
        .sum()
}

fn extract_left_value(s: &str) -> u32 {
    for i in 0..s.len() {
        if let Some(digit) = extract_digit(s, i) {
            return digit;
        }
    }
    panic!("input does not contain digit: `{}`", s)
}

fn extract_right_value(s: &str) -> u32 {
    for i in 0..s.len() {
        if let Some(digit) = extract_digit(s, s.len() - 1 - i) {
            return digit;
        }
    }
    panic!("input does not contain digit: `{}`", s)
}

fn extract_digit(s: &str, start: usize) -> Option<u32> {
    if let Some(v) = extract_1_char(&s[start..start + 1]) {
        return Some(v);
    }
    if start + 3 <= s.len() {
        if let Some(v) = extract_3_char(&s[start..start + 3]) {
            return Some(v);
        }
    }
    if start + 4 <= s.len() {
        if let Some(v) = extract_4_char(&s[start..start + 4]) {
            return Some(v);
        }
    }
    if start + 5 <= s.len() {
        if let Some(v) = extract_5_char(&s[start..start + 5]) {
            return Some(v);
        }
    }
    None
}

fn extract_1_char(s: &str) -> Option<u32> {
    match s {
        "0" => Some(0),
        "1" => Some(1),
        "2" => Some(2),
        "3" => Some(3),
        "4" => Some(4),
        "5" => Some(5),
        "6" => Some(6),
        "7" => Some(7),
        "8" => Some(8),
        "9" => Some(9),
        &_ => None
    }
}

fn extract_3_char(s: &str) -> Option<u32> {
    match s {
        "one" => Some(1),
        "two" => Some(2),
        "six" => Some(6),
        &_ => None
    }
}

fn extract_4_char(s: &str) -> Option<u32> {
    match s {
        "four" => Some(4),
        "five" => Some(5),
        "nine" => Some(9),
        &_ => None
    }
}

fn extract_5_char(s: &str) -> Option<u32> {
    match s {
        "three" => Some(3),
        "seven" => Some(7),
        "eight" => Some(8),
        &_ => None
    }
}

#[cfg(test)]
mod tests {
    use crate::{extract_digit, extract_left_value, extract_right_value, solve};

    #[test]
    fn extract_digit_ok() {
        #[derive(Debug)]
        struct TestCase {
            input: &'static str,
            start: usize,
            want: Option<u32>,
        }

        let test_cases = vec![
            TestCase { input: "0one2three4five6seven8nine", start: 0, want: Some(0) },
            TestCase { input: "0one2three4five6seven8nine", start: 1, want: Some(1) },
            TestCase { input: "0one2three4five6seven8nine", start: 4, want: Some(2) },
            TestCase { input: "0one2three4five6seven8nine", start: 5, want: Some(3) },
            TestCase { input: "0one2three4five6seven8nine", start: 10, want: Some(4) },
            TestCase { input: "0one2three4five6seven8nine", start: 11, want: Some(5) },
            TestCase { input: "0one2three4five6seven8nine", start: 15, want: Some(6) },
            TestCase { input: "0one2three4five6seven8nine", start: 16, want: Some(7) },
            TestCase { input: "0one2three4five6seven8nine", start: 21, want: Some(8) },
            TestCase { input: "0one2three4five6seven8nine", start: 22, want: Some(9) },
            TestCase { input: "1two3four5six7eight9", start: 0, want: Some(1) },
            TestCase { input: "1two3four5six7eight9", start: 1, want: Some(2) },
            TestCase { input: "1two3four5six7eight9", start: 4, want: Some(3) },
            TestCase { input: "1two3four5six7eight9", start: 5, want: Some(4) },
            TestCase { input: "1two3four5six7eight9", start: 9, want: Some(5) },
            TestCase { input: "1two3four5six7eight9", start: 10, want: Some(6) },
            TestCase { input: "1two3four5six7eight9", start: 13, want: Some(7) },
            TestCase { input: "1two3four5six7eight9", start: 14, want: Some(8) },
            TestCase { input: "1two3four5six7eight9", start: 19, want: Some(9) },
        ];
        for tc in test_cases {
            assert_eq!(extract_digit(tc.input, tc.start), tc.want, "failed {:?}", tc)
        }
    }

    #[test]
    fn extract_left_value_ok() {
        #[derive(Debug)]
        struct TestCase {
            input: &'static str,
            want: u32,
        }
        let test_cases = vec![
            TestCase { input: "aaa1ninebbb", want: 1 },
            TestCase { input: "aaanine1bbb", want: 9 },
            TestCase { input: "0", want: 0 },
            TestCase { input: "___six", want: 6 },
            TestCase { input: "seven__", want: 7 },
        ];
        for tc in test_cases {
            assert_eq!(extract_left_value(tc.input), tc.want, "failed {:?}", tc)
        }
    }

    #[test]
    fn extract_right_value_ok() {
        #[derive(Debug)]
        struct TestCase {
            input: &'static str,
            want: u32,
        }
        let test_cases = vec![
            TestCase { input: "aaa1ninebbb", want: 9 },
            TestCase { input: "aaanine1bbb", want: 1 },
            TestCase { input: "0", want: 0 },
            TestCase { input: "___six", want: 6 },
            TestCase { input: "seven__", want: 7 },
        ];
        for tc in test_cases {
            assert_eq!(extract_right_value(tc.input), tc.want, "failed {:?}", tc)
        }
    }

    #[test]
    fn solve_ok() {
        let input = vec!["12", "three0", "ffourr", "zerofive7zero"];
        let sum = solve(input);
        assert_eq!(sum, 12 + 30 + 44 + 57)
    }
}
