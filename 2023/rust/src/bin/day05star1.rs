use std::collections::HashMap;

mod common;

const DEFAULT_PATH: &str = "./input/input-day05.txt";

fn main() {
    let path = common::get_path_from_first_arg(DEFAULT_PATH);
    let content = common::read_file_as_lines(&*path);
    let input = content.iter().map(|s| s.as_str()).collect();
    let sum = solve(input);
    println!("solution = {}", sum);
}

#[derive(Debug, PartialOrd, PartialEq)]
struct Range {
    destination_start: u64,
    source_start: u64,
    size: u64,
}

fn solve(content: Vec<&str>) -> u64 {
    let seeds: Vec<u64> = extract_seeds(&content);

    let mut categories: HashMap<&str, &str> = HashMap::new();
    let mut ranges: HashMap<&str, Vec<Range>> = HashMap::new();
    extract_all_categories_and_ranges(content, &mut categories, &mut ranges);

    seeds.iter()
        .map(|s| map_seed(*s, &categories, &ranges))
        .min()
        .unwrap()
}

fn extract_seeds(content: &Vec<&str>) -> Vec<u64> {
    let seeds_line: &str = content.get(0).expect("seeds line missing");
    let following_line = *content.get(1).expect("seeds line missing");
    if following_line != "" { panic!("second line must be empty"); }
    if !seeds_line.starts_with("seeds: ") { panic!("invalid line: doesn't start with `seeds: `") }
    seeds_line[7..]
        .split(" ")
        .map(|s| s.parse::<u64>().expect(format!("seed must be correct number, got `{}` instead", s).as_str()))
        .collect()
}

fn extract_all_categories_and_ranges<'a>(
    content: Vec<&'a str>,
    categories: &mut HashMap<&'a str, &'a str>,
    ranges: &mut HashMap<&'a str, Vec<Range>>,
) {
    let mut start = 2;
    let mut i = 2;
    loop {
        let maybe_line = content.get(i);
        match maybe_line {
            None => break,
            Some(line) => {
                if line == &"" {
                    let category_lines = &content[start..i];
                    extract_categories_and_ranges(category_lines, categories, ranges);
                    start = i + 1;
                }
            }
        }
        i += 1
    }
    if start != i {
        extract_categories_and_ranges(&content[start..], categories, ranges);
    }
}

fn extract_categories_and_ranges<'a>(
    category_content: &[&'a str],
    categories: &mut HashMap<&'a str, &'a str>,
    ranges: &mut HashMap<&'a str, Vec<Range>>,
) {
    let cat_line = category_content[0];
    let cat_src_from_index = cat_line.find("-to-")
        .expect(format!("couldn't find `-to-` in this line: {}", cat_line).as_str());
    let cat_dst_to_exclusive_index = cat_line.find(" map:")
        .expect(format!("couldn't find ` map:` in this line: {}", cat_line).as_str());

    let src = &cat_line[..cat_src_from_index];
    let dst = &cat_line[cat_src_from_index + 4..cat_dst_to_exclusive_index];
    categories.insert(src, dst);

    let r: Vec<Range> = category_content
        .iter()
        .skip(1)
        .map(|l| extract_numbers(l))
        .collect();
    ranges.insert(src, r);
}

fn extract_numbers(s: &str) -> Range {
    let numbers: Vec<u64> = s.split(" ")
        .map(|n| n.parse::<u64>().expect(format!("invalid number `{}`", n).as_str()))
        .collect();
    Range {
        destination_start: *numbers.get(0).expect("missing destination start"),
        source_start: *numbers.get(1).expect("missing destination start"),
        size: *numbers.get(2).expect("missing destination start"),
    }
}

fn map_seed(
    seed: u64,
    categories: &HashMap<&str, &str>,
    ranges: &HashMap<&str, Vec<Range>>,
) -> u64 {
    // we don't expect more than 100 mapping categories
    let mut loop_breaker = 100;
    let mut cat = "seed";
    let mut src = seed;
    loop {
        let ranges = ranges.get(cat).expect(format!("ranges not found by `{}`", cat).as_str());
        src = map_source_to_destination(src, ranges);
        cat = categories.get(cat).expect(format!("cat not found by `{}`", cat).as_str());
        if !categories.contains_key(cat) { break }
        loop_breaker -= 1;
        if loop_breaker == 0 { panic!("loop_breaker hit"); };
    }
    src
}

fn map_source_to_destination(src: u64, ranges: &Vec<Range>) -> u64 {
    for r in ranges {
        // R{dst: 1, src: 2, size: 3)
        // 2->1
        // 3->2
        // 4->3
        if src >= r.source_start && src < r.source_start + r.size {
            let i = src - r.source_start;
            return i + r.destination_start;
        }
    }
    src
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use crate::*;

    #[test]
    fn test_extract_seeds() {
        let seeds = vec!["seeds: 42 123 666", ""];
        assert_eq!(extract_seeds(&seeds), vec![42, 123, 666]);
    }

    #[test]
    fn test_extract_categories_and_ranges() {
        let content = vec![
            "seed-to-soil map:",
            "50 98 2",
            "52 50 48",
        ];
        let mut categories: HashMap<&str, &str> = HashMap::new();
        let mut ranges: HashMap<&str, Vec<Range>> = HashMap::new();
        extract_categories_and_ranges(&content, &mut categories, &mut ranges);

        assert_eq!(categories, HashMap::from([("seed", "soil")]));
        assert_eq!(ranges.len(), 1, "must contain 1 ranges list");
        assert!(ranges.contains_key("seed"), "must contain `seed` ranges list");
        let seed_ranges = ranges.get("seed").expect("missing seed ranges list");
        assert_eq!(*seed_ranges, vec![
            Range { destination_start: 50, source_start: 98, size: 2 },
            Range { destination_start: 52, source_start: 50, size: 48 },
        ]);
    }

    #[test]
    fn test_extract_all_categories_and_ranges() {
        let content = vec![
            "seeds: 79 14 55 13",
            "",
            "seed-to-soil map:",
            "50 98 2",
            "52 50 48",
            "",
            "soil-to-fertilizer map:",
            "0 15 37",
            "37 52 2",
            "39 0 15",
            "",
            "fertilizer-to-water map:",
            "49 53 8",
            "0 11 42",
            "42 0 7",
            "57 7 4",
            "",
        ];
        let mut categories: HashMap<&str, &str> = HashMap::new();
        let mut ranges: HashMap<&str, Vec<Range>> = HashMap::new();

        extract_all_categories_and_ranges(content, &mut categories, &mut ranges);

        assert_eq!(categories, HashMap::from([
            ("seed", "soil"),
            ("soil", "fertilizer"),
            ("fertilizer", "water")]));
        assert_eq!(ranges.len(), 3, "must contain 3 ranges list");
        let seed_ranges = ranges.get("seed").expect("missing seed ranges list");
        assert_eq!(*seed_ranges, vec![
            Range { destination_start: 50, source_start: 98, size: 2 },
            Range { destination_start: 52, source_start: 50, size: 48 },
        ]);
        let seed_ranges = ranges.get("soil").expect("missing soil ranges list");
        assert_eq!(*seed_ranges, vec![
            Range { destination_start: 0, source_start: 15, size: 37 },
            Range { destination_start: 37, source_start: 52, size: 2 },
            Range { destination_start: 39, source_start: 0, size: 15 },
        ]);
        let seed_ranges = ranges.get("fertilizer").expect("missing fertilizer ranges list");
        assert_eq!(*seed_ranges, vec![
            Range { destination_start: 49, source_start: 53, size: 8 },
            Range { destination_start: 0, source_start: 11, size: 42 },
            Range { destination_start: 42, source_start: 0, size: 7 },
            Range { destination_start: 57, source_start: 7, size: 4 },
        ]);
    }

    #[test]
    fn test_map_source_to_destination() {
        let ranges = vec![
            Range { destination_start: 10, source_start: 20, size: 3 },
            Range { destination_start: 0, source_start: 35, size: 50 },
        ];
        let test_cases = vec![
            (0, 0),
            (19, 19),
            (20, 10),
            (21, 11),
            (22, 12),
            (23, 23),
            (30, 30),
            (34, 34),
            (35, 0),
            (50, 15),
            (84, 49),
            (85, 85),
        ];
        for tc in test_cases {
            let (src, expected_dst) = tc;
            let dst = map_source_to_destination(src, &ranges);
            assert_eq!(dst, expected_dst, "failed for src {}", src);
        }
    }

    #[test]
    fn test_map_seed() {
        let categories = HashMap::from([
            ("seed", "soil"),
            ("soil", "location"),
        ]);
        let ranges = HashMap::from([
            ("seed", vec![Range { destination_start: 100, source_start: 10, size: 3 }]),
            ("soil", vec![Range { destination_start: 50, source_start: 101, size: 4 }]),
        ]);
        let test_cases = vec![
            (1, 1),
            (5, 5),
            (9, 9),
            (10, 100),
            (11, 50),
            (12, 51),
            (13, 13),
            (50, 50),
            (99, 99),
            (100, 100),
            (101, 50),
            (102, 51),
            (103, 52),
            (104, 53),
            (105, 105),
            (106, 106),
        ];
        for tc in test_cases {
            let (src, expected_dst) = tc;
            let dst = map_seed(src, &categories, &ranges);
            assert_eq!(dst, expected_dst, "failed for src {}", src);
        }
    }
}

/*
"seeds: 79 14 55 13",
"",
"seed-to-soil map:",
"50 98 2",
"52 50 48",
"",
"soil-to-fertilizer map:",
"0 15 37",
"37 52 2",
"39 0 15",
"",
"fertilizer-to-water map:",
"49 53 8",
"0 11 42",
"42 0 7",
"57 7 4",
"",
"water-to-light map:",
"88 18 7",
"18 25 70",
"",
"light-to-temperature map:",
"45 77 23",
"81 45 19",
"68 64 13",
"",
"temperature-to-humidity map:",
"0 69 1",
"1 0 69",
"",
"humidity-to-location map:",
"60 56 37",
"56 93 4",
 */
