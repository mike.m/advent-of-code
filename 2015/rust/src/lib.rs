use std::env::args;
use std::fs::read_to_string;
use std::io::Error;

pub fn read_input_single_line(default_path: &str) -> Result<String, Error> {
    let p = path(default_path);
    read_to_string(p)
}

pub fn read_input_multiple_lines(default_path: &str) -> Result<Vec<String>, Error> {
    let p = path(default_path);
    let s = read_to_string(p)?;
    let res = s.lines().map(|it| it.to_string()).collect();
    Ok(res)
}

fn path(default: &str) -> String {
    let p = match args().len() {
        2 => args().nth(1).unwrap(),
        _ => String::from(default),
    };
    println!("PATH: {}", p);
    p
}
