fn main() {
    match aoc2015::read_input_single_line("../input-day04.txt") {
        Ok(s) => println!("SOLUTION: {}", solve(s)),
        Err(e) => eprintln!("Failed to solve the problem: {}", e),
    };
}

fn solve(prefix: String) -> i32 {
    let mut i = 0;
    while i < 99999999 {
        let full = format!("{}{}", prefix, i);
        let text = full.as_bytes();
        let digest = md5::compute(text);
        if digest.0[0] == 0 && digest.0[1] == 0 && digest.0[2] == 0 {
            return i;
        }
        i += 1;
    }
    panic!("failed to find proper hash")
}
