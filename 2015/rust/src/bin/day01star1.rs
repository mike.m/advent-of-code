fn main() {
    match aoc2015::read_input_single_line("../input-day01.txt") {
        Ok(s) => println!("SOLUTION: {}", solve(&s)),
        Err(e) => eprintln!("Failed to solve the problem: {}", e),
    };
}

fn solve(input: &str) -> i32 {
    let mut f = 0;
    for c in input.chars() {
        match c {
            '(' => f += 1,
            ')' => f -= 1,
            _ => panic!("invalid input character: {}", c)
        }
    }
    f
}
