fn main() {
    match aoc2015::read_input_multiple_lines("../input-day05.txt") {
        Ok(s) => println!("SOLUTION: {}", solve(s)),
        Err(e) => eprintln!("Failed to solve the problem: {}", e),
    };
}

fn solve(input: Vec<String>) -> i32 {
    input.iter().filter(|s| is_nice(s)).count() as i32
}

fn is_nice(s: &str) -> bool {
    contains_3_vowels(&s) && contains_double(&s) && not_contains_special_pairs(&s)
}

// It contains at least three vowels (aeiou only)
fn contains_3_vowels(s: &str) -> bool {
    let mut v = 0;
    for c in s.chars() {
        if c == 'a' || c == 'e' || c == 'o' || c == 'u' || c == 'i' {
            v += 1
        }
        if v == 3 {
            return true;
        }
    }
    false
}

// It contains at least one letter that appears twice in a row
fn contains_double(s: &str) -> bool {
    let mut chars = s.chars();
    let mut prev = chars.next().unwrap();
    for c in chars {
        if c == prev {
            return true;
        }
        prev = c
    }
    false
}

// It does not contain the strings ab, cd, pq, or xy
fn not_contains_special_pairs(s: &str) -> bool {
    let mut chars = s.chars();
    let mut prev = chars.next().unwrap();
    for c in chars {
        if prev == 'a' && c == 'b'
            || prev == 'c' && c == 'd'
            || prev == 'p' && c == 'q'
            || prev == 'x' && c == 'y' {
            return false;
        }
        prev = c
    }
    true
}

#[test]
fn test_contains_3_vowels() {
    assert!(contains_3_vowels("aeo"));
    assert!(contains_3_vowels("oui"));
    assert!(contains_3_vowels("bbbaaabbb"));
    assert!(!contains_3_vowels("bbbaabbb"));
}

#[test]
fn test_contains_double() {
    assert!(contains_double("qwertyy"));
    assert!(contains_double("qqwerty"));
    assert!(contains_double("aaa"));
    assert!(!contains_double("abcdefg"));
    assert!(!contains_double("ababab"));
}

#[test]
fn test_not_contains_special_pairs() {
    assert!(not_contains_special_pairs("hhhhhh"));
    assert!(!not_contains_special_pairs("hhhhhhab"));
    assert!(!not_contains_special_pairs("hhhhhhcd"));
    assert!(!not_contains_special_pairs("hhhhhhpq"));
    assert!(!not_contains_special_pairs("hhhhhhxy"));
}

#[test]
fn test_is_nice_ugknbfddgicrmopn() {
    assert!(is_nice("ugknbfddgicrmopn"));
    assert!(is_nice("aaa"));
    assert!(!is_nice("jchzalrnumimnmhp"));
    assert!(!is_nice("haegwjzuvuyypxyu"));
    assert!(!is_nice("dvszwmarrgswjxmb"));
}
