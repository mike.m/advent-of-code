fn main() {
    match aoc2015::read_input_multiple_lines("../input-day02.txt") {
        Ok(s) => println!("SOLUTION: {}", solve(s)),
        Err(e) => eprintln!("Failed to solve the problem: {}", e),
    };
}

fn solve(input: Vec<String>) -> i32 {
    let mut area = 0;
    for line in input {
        let mut vals: Vec<i32> = line
            .split("x")
            .map(|s| s.to_string().parse::<i32>().expect("must be a number"))
            .collect();
        if vals.len() != 3 {
            panic!("invalid input, must be AxBxC, where A, B and C are numbers")
        }
        vals.sort_unstable();
        let shortest = vals.remove(0);
        let middle = vals.remove(0);
        let longest = vals.remove(0);
        area += 3 * shortest * middle // adding a slack, that's why 3 times multiplication
            + 2 * middle * longest
            + 2 * shortest * longest
    }
    area
}
