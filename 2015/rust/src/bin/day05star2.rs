fn main() {
    match aoc2015::read_input_multiple_lines("../input-day05.txt") {
        Ok(s) => println!("SOLUTION: {}", solve(s)),
        Err(e) => eprintln!("Failed to solve the problem: {}", e),
    };
}

fn solve(input: Vec<String>) -> i32 {
    input.iter().filter(|s| is_nice(s)).count() as i32
}

fn is_nice(s: &str) -> bool {
    contains_pair_double(&s) && contains_repeated_char(&s)
}

// It contains a pair of any two letters that appears at least twice in the string without overlapping
fn contains_pair_double(s: &str) -> bool {
    s.chars().zip(s.chars().skip(1)).enumerate().any(|(i, (a, b))| pair_double(s, i, a, b))
}

// s - full string
// i - index of current pair to check
// c1,c2 - current pair to check
//
// There must not be overlapping so we must skip current pair, also there is no point of checking previous pairs, so we just start checking from after current pair.
fn pair_double(s: &str, i: usize, c1: char, c2: char) -> bool {
    s.chars().skip(i + 2).zip(s.chars().skip(i + 3)).any(|(a, b)| a == c1 && b == c2)
}

// It contains at least one letter which repeats with exactly one letter between them
fn contains_repeated_char(s: &str) -> bool {
    s.chars().skip(2).zip(s.chars()).any(|(a, b)| a == b)
}

#[test]
fn test_contains_pair_double() {
    assert!(contains_pair_double("xyxy"));
    assert!(contains_pair_double("aabcdefgaa"));
    assert!(!contains_pair_double("aaa"));
}

#[test]
fn test_contains_repeated_char() {
    assert!(contains_repeated_char("xyx"));
    assert!(contains_repeated_char("abcdefeghi"));
    assert!(contains_repeated_char("aaa"));
    assert!(!contains_repeated_char("aabb"));
}

#[test]
fn test_is_nice() {
    assert!(is_nice("qjhvhtzxzqqjkmpb"));
    assert!(is_nice("xxyxx"));
    assert!(!is_nice("uurcxstgmygtbstg"));
    assert!(!is_nice("ieodomkazucvgmuy"));
}