use std::collections::HashSet;

fn main() {
    match aoc2015::read_input_single_line("../input-day03.txt") {
        Ok(s) => println!("SOLUTION: {}", solve(s)),
        Err(e) => eprintln!("Failed to solve the problem: {}", e),
    };
}

#[derive(Debug, PartialEq, Eq, Hash, Copy, Clone)]
struct House {
    x: i32,
    y: i32,
}

#[derive(Debug)]
struct Member {
    position: House,
}

impl Member {
    fn new() -> Self {
        Member {
            position: House { x: 0, y: 0 },
        }
    }

    fn go(&mut self, direction: char) -> House {
        let current_x = self.position.x;
        let current_y = self.position.y;
        self.position = match direction {
            '>' => House { x: current_x + 1, y: current_y },
            '<' => House { x: current_x - 1, y: current_y },
            '^' => House { x: current_x, y: current_y + 1 },
            'v' => House { x: current_x, y: current_y - 1 },
            _ => panic!("invalid character, expected one of '<>^v', check the input file")
        };
        self.position
    }
}

struct Team<'a> {
    members: Vec<&'a mut Member>,
    index: usize,
    visited: HashSet<House>,
}

impl<'a> Team<'a> {
    fn new() -> Team<'a> {
        Team {
            members: Vec::new(),
            index: 0,
            visited: HashSet::new(),
        }
    }

    fn add_member(&mut self, actor: &'a mut Member) {
        self.members.push(actor)
    }

    fn go(&mut self, direction: char) {
        let i = self.index;
        let members = &mut self.members;
        let member = &mut members[i];
        let house = member.go(direction);
        self.visited.insert(house);

        // Choose next member
        self.index = (i + 1) % members.len();
    }

    fn count_houses(&self) -> usize {
        self.visited.len()
    }
}

fn solve(input: String) -> usize {
    let mut team = Team::new();
    let mut santa = Member::new();
    team.add_member(&mut santa);
    let mut robo_santa = Member::new();
    team.add_member(&mut robo_santa);
    for c in input.chars() {
        team.go(c)
    }
    team.count_houses()
}