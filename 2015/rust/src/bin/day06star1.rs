use std::collections::HashSet;

use regex::{Captures, Regex};

fn main() {
    match aoc2015::read_input_multiple_lines("../input-day06.txt") {
        Ok(s) => println!("SOLUTION: {}", solve(s)),
        Err(e) => eprintln!("Failed to solve the problem: {}", e),
    };
}

#[derive(Debug, Copy, Clone, PartialEq)]
struct FromTo {
    x_from: i32,
    y_from: i32,
    x_to: i32,
    y_to: i32,
}

#[derive(Debug, PartialEq)]
enum Command {
    TurnOn(FromTo),
    TurnOff(FromTo),
    Toggle(FromTo),
}

fn solve(input: Vec<String>) -> i32 {
    let pattern = get_pattern();
    let mut lights: HashSet<(i32, i32)> = HashSet::new();
    for line in input {
        let c = parse_command(&line, &pattern);
        match c {
            Command::TurnOn(ft) => {
                let mut x = ft.x_from;
                while x <= ft.x_to {
                    let mut y = ft.y_from;
                    while y <= ft.y_to {
                        lights.insert((x, y));
                        y += 1;
                    }
                    x += 1;
                }
            }
            Command::TurnOff(ft) => {
                let mut x = ft.x_from;
                while x <= ft.x_to {
                    let mut y = ft.y_from;
                    while y <= ft.y_to {
                        lights.remove(&(x, y));
                        y += 1;
                    }
                    x += 1;
                }
            }
            Command::Toggle(ft) => {
                let mut x = ft.x_from;
                while x <= ft.x_to {
                    let mut y = ft.y_from;
                    while y <= ft.y_to {
                        if lights.contains(&(x, y)) {
                            lights.remove(&(x, y));
                        } else {
                            lights.insert((x, y));
                        }
                        y += 1;
                    }
                    x += 1;
                }
            }
        }
    }
    lights.len() as i32
}

fn get_pattern() -> Regex {
    Regex::new(r"^(turn on|turn off|toggle) (\d+),(\d+) through (\d+),(\d+)$").unwrap()
}

fn parse_command(line: &str, pattern: &Regex) -> Command {
    return match pattern.captures(line) {
        Some(a) => {
            let action = &a[1];
            match action {
                "turn on" => Command::TurnOn(parse_from_to(&a)),
                "turn off" => Command::TurnOff(parse_from_to(&a)),
                "toggle" => Command::Toggle(parse_from_to(&a)),
                _ => panic!("unknown command's action: {}", action),
            }
        }
        _ => panic!("unexpected input: {}", line),
    };
}

fn parse_from_to(captures: &Captures) -> FromTo {
    let x1 = i32::from_str_radix(&captures[2], 10).expect(format!("failed to parse FromTo: {:?}", captures).as_str());
    let y1 = i32::from_str_radix(&captures[3], 10).expect(format!("failed to parse FromTo: {:?}", captures).as_str());
    let x2 = i32::from_str_radix(&captures[4], 10).expect(format!("failed to parse FromTo: {:?}", captures).as_str());
    let y2 = i32::from_str_radix(&captures[5], 10).expect(format!("failed to parse FromTo: {:?}", captures).as_str());
    FromTo {
        x_from: x1,
        y_from: y1,
        x_to: x2,
        y_to: y2,
    }
}

#[test]
fn test_parse_command_turn_on() {
    let pattern = get_pattern();
    let got = parse_command("turn on 1,2 through 3,4", &pattern);
    let want = Command::TurnOn(FromTo { x_from: 1, y_from: 2, x_to: 3, y_to: 4 });
    assert_eq!(got, want);
}

#[test]
fn test_parse_command_turn_off() {
    let pattern = get_pattern();
    let want = parse_command("turn off 11,12 through 13,14", &pattern);
    let got = Command::TurnOff(FromTo { x_from: 11, y_from: 12, x_to: 13, y_to: 14 });
    assert_eq!(want, got);
}

#[test]
fn test_parse_command_toggle() {
    let pattern = get_pattern();
    let got = parse_command("toggle 21,22 through 23,24", &pattern);
    let want = Command::Toggle(FromTo { x_from: 21, y_from: 22, x_to: 23, y_to: 24 });
    assert_eq!(got, want);
}
