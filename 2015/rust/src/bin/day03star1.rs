use std::collections::HashSet;

fn main() {
    match aoc2015::read_input_single_line("../input-day03.txt") {
        Ok(s) => println!("SOLUTION: {}", solve(s)),
        Err(e) => eprintln!("Failed to solve the problem: {}", e),
    };
}

#[derive(PartialEq, Eq, Hash, Copy, Clone)]
struct House {
    x: i32,
    y: i32,
}

fn solve(input: String) -> usize {
    let mut houses = HashSet::new();
    let mut position = House { x: 0, y: 0 };
    houses.insert(position);
    for c in input.chars() {
        match c {
            '>' => position = House { x: position.x + 1, y: position.y },
            '<' => position = House { x: position.x - 1, y: position.y },
            '^' => position = House { x: position.x, y: position.y + 1 },
            'v' => position = House { x: position.x, y: position.y - 1 },
            _ => panic!("invalid character, expected one of '<>^v', check the input file")
        }
        houses.insert(position);
    }
    return houses.len();
}
