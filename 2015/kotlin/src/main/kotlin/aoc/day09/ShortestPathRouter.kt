package aoc.day09

class ShortestPathRouter {
    fun shortestDistance(routes: Map<String, Set<Pair<String, Int>>>): Int {
        return routes
                .keys
                .map {
                    travelShortest(routes, listOf(it), 0)
                }
                .filter { it != -1 }
                .min() ?: throw IllegalStateException("Could not find shortest distance covering all location")
    }

    private fun travelShortest(routes: Map<String, Set<Pair<String, Int>>>, visited: List<String>, distance: Int): Int {
        return if (visited.size == routes.size) {
            distance
        } else {
            val loc = visited.last()
            routes[loc]!!
                    .filterNot { visited.contains(it.first) }
                    .map { (nextLoc, nextLocDist) ->
                        travelShortest(routes, visited + nextLoc, distance + nextLocDist)
                    }
                    .filterNot { it == -1 }
                    .min() ?: -1
        }
    }
}
