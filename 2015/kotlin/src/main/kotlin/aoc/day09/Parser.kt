package aoc.day09

import java.util.regex.Pattern

private val p = Pattern.compile("(\\w+) to (\\w+) = (\\d+)")

internal class Parser {
    val locations = mutableMapOf<String, Boolean>()
    val routes = mutableMapOf<String, MutableSet<Pair<String, Int>>>()

    fun addRoutes(routes: List<String>) {
        routes.forEachIndexed { i, r ->
            try {
                addRoute(r)
            } catch (e: Exception) {
                throw IllegalStateException("Failed to parse line ${i + 1}", e)
            }
        }
    }

    private fun addRoute(str: String) {
        val m = p.matcher(str)
        if (m.find()) {
            val source = m.group(1)
            val destination = m.group(2)
            locations.putIfAbsent(source, false)
            locations.putIfAbsent(destination, false)
            val distance = m.group(3).toInt()

            // There
            val routeThere = routes.getOrPut(source) { mutableSetOf() }
            routeThere.add(Pair(destination, distance))

            // And back
            val routeBack = routes.getOrPut(destination) { mutableSetOf() }
            routeBack.add(Pair(source, distance))
        } else {
            throw IllegalStateException("Could not parse a string: $str")
        }
    }
}
