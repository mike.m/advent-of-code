package aoc.day09

import aoc.readInputLines

fun main(args: Array<String>) {
    val input = readInputLines(args, "input-day09.txt")
    val totalWrapping = Day09Star2().process(input)
    println(totalWrapping)
}

internal class Day09Star2 {
    fun process(lines: List<String>): Int {
        val p = Parser()
        p.addRoutes(lines)
        return LongestPathRouter().longestDistance(p.routes)
    }
}
