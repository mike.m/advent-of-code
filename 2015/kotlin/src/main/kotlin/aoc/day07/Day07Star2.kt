package aoc.day07

import aoc.readInputLines

fun main(args: Array<String>) {
    val input = readInputLines(args, "input-day07.txt")
    val totalWrapping = Day07Star2().process(input)
    println(totalWrapping)
}

class Day07Star2 {
    fun process(lines: List<String>): Int {
        val overrideValue = Day07Star1().process(lines)

        val p = Processor(lines)
        p.gates
                .filter { it is Assign }
                .map { it as Assign }
                .filter { it.output.name == "b" }
                .forEach { it.input.value = overrideValue }

        p.execute()

        // Print results
        p.gates.forEach { println(it) }
        println()
        p.signals.toSortedMap().forEach { println(it) }

        val a = p.signals["a"] ?: throw IllegalStateException("Signal 'a' not found")
        return a.value ?: throw IllegalStateException("Signal 'a' has not been initialized")
    }
}
