package aoc.day07

internal class Parser {
    fun parse(signals: MutableMap<String, Signal>, line: String): GenericGate {
        val tokens = line.split(" ").map { parseToken(it) }
        //a -> b
        //NOT a -> b
        //a AND b -> c
        //a OR b -> c
        //a RSHIFT b -> c
        //a LSHIFT b -> c
        return when {
            isAssignment(tokens) -> Assign(
                    getSignal(signals, tokens[0]),
                    getVariable(signals, tokens[2]))

            isNot(tokens) -> Not(
                    getSignal(signals, tokens[1]),
                    getVariable(signals, tokens[3]))

            isAnd(tokens) -> And(
                    getSignal(signals, tokens[0]),
                    getSignal(signals, tokens[2]),
                    getVariable(signals, tokens[4]))

            isOr(tokens) -> Or(
                    getSignal(signals, tokens[0]),
                    getSignal(signals, tokens[2]),
                    getVariable(signals, tokens[4]))

            isShiftRight(tokens) -> ShiftRight(
                    getSignal(signals, tokens[0]),
                    getSignal(signals, tokens[2]),
                    getVariable(signals, tokens[4]))

            isShiftLeft(tokens) -> ShiftLeft(
                    getSignal(signals, tokens[0]),
                    getSignal(signals, tokens[2]),
                    getVariable(signals, tokens[4]))

            else -> throw IllegalStateException("Gate not recognized: $tokens")
        }
    }

    private fun getSignal(signals: MutableMap<String, Signal>, t: Token): Signal {
        return if (t.type == TokenType.NUMBER) {
            Number(t.text.toInt())
        } else {
            getVariable(signals, t)
        }
    }

    private fun getVariable(signals: MutableMap<String, Signal>, token: Token): Variable {
        val name = token.text
        val defaultValue = { Variable(name) }
        val signal = signals.getOrPut(name, defaultValue)
        return when (signal) {
            is Variable -> signal
            else -> throw IllegalStateException("Not a variable: $signal")
        }
    }

    private fun isAssignment(tokens: List<Token>) = tokens.size == 3
            && isNumberOrSignal(tokens[0])
            && tokens[1].type == TokenType.ARROW_OPERATOR
            && tokens[2].type == TokenType.SIGNAL_NAME

    private fun isNot(tokens: List<Token>) = tokens.size == 4
            && tokens[0].type == TokenType.NOT
            && isNumberOrSignal(tokens[1])
            && tokens[2].type == TokenType.ARROW_OPERATOR
            && isNumberOrSignal(tokens[3])

    private fun isAnd(tokens: List<Token>) = isDoubleInputGate(tokens, TokenType.AND)

    private fun isOr(tokens: List<Token>) = isDoubleInputGate(tokens, TokenType.OR)

    private fun isShiftRight(tokens: List<Token>) = isDoubleInputGate(tokens, TokenType.RSHIFT)

    private fun isShiftLeft(tokens: List<Token>) = isDoubleInputGate(tokens, TokenType.LSHIFT)

    private fun isDoubleInputGate(tokens: List<Token>, operatorType: TokenType) = tokens.size == 5
            && isNumberOrSignal(tokens[0])
            && tokens[1].type == operatorType
            && isNumberOrSignal(tokens[2])
            && tokens[3].type == TokenType.ARROW_OPERATOR
            && isNumberOrSignal(tokens[4])

    private fun isNumberOrSignal(t: Token) =
            t.type == TokenType.NUMBER || t.type == TokenType.SIGNAL_NAME

}

enum class TokenType { NOT, AND, OR, RSHIFT, LSHIFT, SIGNAL_NAME, NUMBER, ARROW_OPERATOR }

internal data class Token(val text: String, val type: TokenType)

internal fun parseToken(text: String): Token {
    val type = when {
        text == "AND" -> TokenType.AND
        text == "OR" -> TokenType.OR
        text == "NOT" -> TokenType.NOT
        text == "RSHIFT" -> TokenType.RSHIFT
        text == "LSHIFT" -> TokenType.LSHIFT
        text == "->" -> TokenType.ARROW_OPERATOR
        text.isNumber() -> TokenType.NUMBER
        text.isSignalName() -> TokenType.SIGNAL_NAME
        else -> throw IllegalStateException("Could not parse token: $text")
    }
    return Token(text, type)
}

private fun String.isNumber() = this.all { it in '0'..'9' }

private fun String.isSignalName() = this.all { it in 'a'..'z' }
