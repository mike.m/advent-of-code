package aoc.day07

abstract class GenericGate {

    var executed = false

    fun execute() {
        if (!executed && executeInternal()) {
            executed = true
            enableOutput()
        }
    }

    abstract fun enableOutput()

    abstract fun executeInternal(): Boolean
}

/*******************************************************
 * And.
 */
internal class And(private val left: Signal,
                   private val right: Signal,
                   private val output: Variable) : GenericGate() {

    init {
        left.register(this)
        right.register(this)
    }

    override fun executeInternal(): Boolean {
        val l = left.value ?: return false
        val r = right.value ?: return false
        output.value = l and r
        return true
    }

    override fun enableOutput() {
        output.enable()
    }

    override fun toString() = "And(left=$left, right=$right, output=$output)"
}

/*******************************************************
 * Or.
 */
internal class Or(private val left: Signal,
                  private val right: Signal,
                  private val output: Variable) : GenericGate() {

    init {
        left.register(this)
        right.register(this)
    }

    override fun executeInternal(): Boolean {
        val l = left.value ?: return false
        val r = right.value ?: return false
        output.value = l or r
        return true
    }

    override fun enableOutput() {
        output.enable()
    }

    override fun toString() = "Or(left=$left, right=$right, output=$output)"
}

/*******************************************************
 * Not.
 */
internal class Not(private val input: Signal,
                   private val output: Variable) : GenericGate() {

    init {
        input.register(this)
    }

    override fun executeInternal(): Boolean {
        val i = input.value ?: return false
        output.value = i.inv() and 0xffff
        return true
    }

    override fun enableOutput() {
        output.enable()
    }

    override fun toString() = "Not(input=$input, output=$output)"
}

/*******************************************************
 * Assign.
 */
internal class Assign(val input: Signal,
                      val output: Variable) : GenericGate() {

    init {
        input.register(this)
    }

    override fun executeInternal(): Boolean {
        val i = input.value ?: return false
        output.value = i
        return true
    }

    override fun enableOutput() {
        output.enable()
    }

    override fun toString() = "Assign(input=$input, output=$output)"
}

/*******************************************************
 * Shift right.
 */
internal class ShiftRight(private val input: Signal,
                          private val offset: Signal,
                          private val output: Variable) : GenericGate() {

    init {
        input.register(this)
        offset.register(this)
    }

    override fun executeInternal(): Boolean {
        val i = input.value ?: return false
        val o = offset.value ?: return false
        output.value = i shr o and 0xffff
        return true
    }

    override fun enableOutput() {
        output.enable()
    }

    override fun toString() = "ShiftRight(input=$input, offset=$offset, output=$output)"
}

/*******************************************************
 * Shift left.
 */
internal class ShiftLeft(private val input: Signal,
                         private val offset: Signal,
                         private val output: Variable) : GenericGate() {

    init {
        input.register(this)
    }

    override fun executeInternal(): Boolean {
        val i = input.value ?: return false
        val o = offset.value ?: return false
        output.value = i shl o and 0xffff
        return true
    }

    override fun enableOutput() {
        output.enable()
    }

    override fun toString() = "ShiftLeft(input=$input, offset=$offset, output=$output)"
}
