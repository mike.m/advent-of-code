package aoc.day07

class Processor(lines: List<String>) {
    private val p = Parser()
    val signals = mutableMapOf<String, Signal>()
    val gates: MutableList<GenericGate>

    init {
        gates = lines
                .mapIndexed { i, l ->
                    try {
                        p.parse(signals, l)
                    } catch (e: IllegalStateException) {
                        throw IllegalStateException("Could not parse line: ${i + 1}", e)
                    }
                }
                .toMutableList()
    }

    fun execute() = gates
            .filter { it is Assign }
            .map { it as Assign }
            .filter { it.input is Number }
            .forEach { it.execute() }
}