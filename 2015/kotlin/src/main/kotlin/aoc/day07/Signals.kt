package aoc.day07

abstract class Signal(var value: Int? = null) {

    private val gates: MutableSet<GenericGate> = mutableSetOf()

    fun register(gate: GenericGate) {
        val added = gates.add(gate)
        if (!added) {
            throw IllegalStateException("WARN! Registration failed due to gate duplication: Gate=$gate")
        }
    }

    fun enable() {
        preEnable()
        gates.filterNot { it.executed }
                .forEach { it.execute() }
    }

    internal abstract fun preEnable()
}

internal class Variable(internal val name: String) : Signal() {

    override fun preEnable() {
        value ?: throw IllegalStateException("Value not set")
    }

    override fun toString() = "Variable(name='$name', value=$value)"
}

internal class Number(n: Int) : Signal(n) {
    override fun preEnable() {
        // Nothing.
    }

    override fun toString() = "Number(value=$value)"
}
