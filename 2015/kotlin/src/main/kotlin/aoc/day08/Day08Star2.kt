package aoc.day08

import aoc.readInputLines

fun main(args: Array<String>) {
    val input = readInputLines(args, "input-day08.txt")
    val totalWrapping = Day08Star2().process(input)
    println(totalWrapping)
}

internal class Day08Star2 {
    fun process(lines: List<String>): Int {
        val original = lines.map { it.length }.sum()
        val encoded = lines.map { encodedLength(it) }.sum()
        return encoded - original
    }
}

internal fun encodedLength(str: String): Int = str
        .map {
            when (it) {
                '"' -> 2
                '\\' -> 2
                else -> 1
            }
        }
        .sum() + 2 /* for quotes */
