package aoc.day08

/**
 * Must start and end double quote.
 */
internal class LengthCalculator {
    fun memoryLength(str: String): Int {

        if (str.first() != '"' || str.last() != '"')
            throw IllegalStateException("String doesn't start or end with double quote")

        var memoryLength = 0
        var i = 1
        var tempLength = -1
        while (tempLength != 0) {
            tempLength = next(str, i)
            if (tempLength != 0) {
                i += tempLength
                memoryLength++
            }
        }

        return memoryLength
    }

    /**
     * Number of string characters that evaluates to one memory character at given index.
     *
     * @param str String.
     * @param index Starting index.
     * @return Number of characters.
     */
    private fun next(str: String, index: Int): Int {
        val c = str[index]
        return when {
            isChar(c) -> 1
            c == '\\' -> escaped(str, index + 1)
            c == '"' -> 0
            else -> throw IllegalStateException("Letter or backslash expected: Got $c at ${index + 1}")
        }
    }

    private fun escaped(str: String, index: Int): Int {
        val c = str[index]
        return when (c) {
            '\\' -> 2
            '"' -> 2
            'x' -> escapedHex(str, index + 1)
            else -> throw IllegalStateException("Expected backslash, letter or number: Got $c at ${index + 1}")
        }
    }

    private fun escapedHex(str: String, index: Int): Int {
        val c = str[index]
        val c2 = str[index + 1]
        return if (isHexDigit(c) && isHexDigit(c2)) {
            4
        } else {
            throw IllegalStateException("Expected hex number: Got $c$c2 at index ${index + 1}")
        }
    }

    private fun isChar(char: Char) = char in 'a'..'z' || char in '0'..'9'

    private fun isHexDigit(char: Char) = char in 'a'..'f' || char in '0'..'9'
}
