package aoc.day08

import aoc.readInputLines

fun main(args: Array<String>) {
    val input = readInputLines(args, "input-day08.txt")
    val totalWrapping = Day08Star1().process(input)
    println(totalWrapping)
}

internal class Day08Star1 {
    fun process(lines: List<String>): Int {
        val lengthCalculator = LengthCalculator()
        val literalLength = lines.map { it.length }.sum()
        val inMemoryLength = lines
                .mapIndexed { i, l ->
                    try {
                        lengthCalculator.memoryLength(l)
                    } catch (e: Exception) {
                        throw IllegalStateException("Failed to parse line ${i + 1}: `$l`", e)
                    }
                }
                .sum()
        return literalLength - inMemoryLength
    }
}
