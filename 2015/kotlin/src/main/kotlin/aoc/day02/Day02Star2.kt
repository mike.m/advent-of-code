package aoc.day02

import aoc.readInputLines

fun main(args: Array<String>) {
    val input = readInputLines(args, "input-day02.txt")
    val totalRibbon = Day02Star2().process(input)
    println(totalRibbon)
}

class Day02Star2 {
    fun process(input: List<String>) = input
            .map { it.split("x").map { it.toInt() } }
            .map { (l, w, h) ->
                val sorted = listOf(l, w, h).sorted()
                val ribbonBow = l * w * h
                sorted[0] * 2 + sorted[1] * 2 + ribbonBow
            }
            .reduce { acc, i -> acc + i }
}
