package aoc.day02

import aoc.readInputLines
import kotlin.math.min

fun main(args: Array<String>) {
    val input = readInputLines(args, "input-day02.txt")
    val totalWrapping = Day02Star1().process(input)
    println(totalWrapping)
}

class Day02Star1 {
    fun process(input: List<String>) = input
            .map { it.split("x").map { it.toInt() } }
            .map { (l, w, h) ->
                val lw = l * w
                val lh = l * h
                val wh = w * h
                val smallestSide = min(min(lw, lh), wh)
                2 * lw + 2 * lh + 2 * wh + smallestSide
            }
            .reduce { acc, i -> acc + i }
}
