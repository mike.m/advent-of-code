package aoc.day06

val toggle: (Boolean) -> Boolean = { !it }

val on: (Boolean) -> Boolean = { true }

val off: (Boolean) -> Boolean = { false }

val addOne: (Int) -> Int = { it + 1 }

val addTwo: (Int) -> Int = { it + 2 }

val subOne: (Int) -> Int = {
    if (it > 0) {
        it - 1
    } else {
        it
    }
}

enum class Action(val executeToggle: (Boolean) -> Boolean, val executeGradient: (Int) -> Int) {
    TOGGLE(toggle, addTwo),
    TURN_ON(on, addOne),
    TURN_OFF(off, subOne)
}

fun getAction(str: String) = when (str) {
    "toggle" -> Action.TOGGLE
    "turn on" -> Action.TURN_ON
    "turn off" -> Action.TURN_OFF
    else -> throw IllegalStateException("No matching command type: $str")
}
