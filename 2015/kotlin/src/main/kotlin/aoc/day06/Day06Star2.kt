package aoc.day06

import aoc.readInputLines

fun main(args: Array<String>) {
    val input = readInputLines(args, "input-day06.txt")
    val count = Day06Star2(1000).process(input)
    println(count)
}

fun executeGradientCommand(lights: IntArray, c: Command, size: Int) {
    val w = c.to.first - c.from.first
    val h = c.to.second - c.from.second
    var cursor = c.from.first + c.from.second * size
    (0..h).forEach {
        (0..w).forEach {
            lights[cursor] = c.action.executeGradient(lights[cursor])
            cursor++
        }
        cursor += size - w - 1
    }
}

class Day06Star2(private val size: Int) {
    fun process(input: List<String>): Int {
        val lights = IntArray(size * size) { 0 }
        input.map { it.parseCommand() }
                .forEach { executeGradientCommand(lights, it, size) }
        return lights.sum()
    }
}
