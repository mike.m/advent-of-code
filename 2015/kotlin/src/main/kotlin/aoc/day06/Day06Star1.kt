package aoc.day06

import aoc.readInputLines

fun main(args: Array<String>) {
    val input = readInputLines(args, "input-day06.txt")
    val count = Day06Star1(1000).process(input)
    println(count)
}

fun executeToggleCommand(lights: BooleanArray, c: Command, size: Int) {
    val w = c.to.first - c.from.first
    val h = c.to.second - c.from.second
    var cursor = c.from.first + c.from.second * size
    (0..h).forEach {
        (0..w).forEach {
            lights[cursor] = c.action.executeToggle(lights[cursor])
            cursor++
        }
        cursor += size - w - 1
    }
}

class Day06Star1(private val size: Int) {
    fun process(input: List<String>): Int {
        val lights = BooleanArray(size * size) { false }
        input.map { it.parseCommand() }
                .forEach { executeToggleCommand(lights, it, size) }
        return lights.count { it }
    }
}
