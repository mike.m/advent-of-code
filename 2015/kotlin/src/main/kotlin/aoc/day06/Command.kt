package aoc.day06

import java.util.regex.Pattern

data class Command(val action: Action, val from: Pair<Int, Int>, val to: Pair<Int, Int>)

private val p = Pattern.compile("(toggle|turn on|turn off) (\\d+),(\\d+) through (\\d+),(\\d+)")!!

fun String.parseCommand(): Command {
    val m = p.matcher(this)
    return if (m.matches()) {
        Command(
                getAction(m.group(1)),
                Pair(m.group(2).toInt(), m.group(3).toInt()),
                Pair(m.group(4).toInt(), m.group(5).toInt())
        )
    } else {
        throw IllegalStateException("No match: $this")
    }
}
