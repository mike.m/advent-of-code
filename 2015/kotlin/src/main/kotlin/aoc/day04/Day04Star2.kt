package aoc.day04

import aoc.readInputOneLine

fun main(args: Array<String>) {
    val input = readInputOneLine(args, "input-day04.txt")
    val floor = Day04Star2().process(input)
    println(floor)
}

class Day04Star2 {
    fun process(input: String): Int {
        return generateSequence(0) { it + 1 }
                .map { md5("$input$it") }
                .indexOfFirst { it.startsWith("000000") }
    }
}
