package aoc.day04

import aoc.readInputOneLine
import java.security.MessageDigest
import javax.xml.bind.DatatypeConverter

fun main(args: Array<String>) {
    val input = readInputOneLine(args, "input-day04.txt")
    val floor = Day04Star1().process(input)
    println(floor)
}

fun md5(text: String): String {
    val md = MessageDigest.getInstance("MD5")
    val hash = md.digest(text.toByteArray())
    return DatatypeConverter.printHexBinary(hash)
}

class Day04Star1 {
    fun process(input: String): Int {
        return generateSequence(0) { it + 1 }
                .map { md5("$input$it") }
                .indexOfFirst { it.startsWith("00000") }
    }
}
