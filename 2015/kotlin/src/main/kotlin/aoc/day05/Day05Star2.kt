package aoc.day05

import aoc.readInputLines

fun main(args: Array<String>) {
    val input = readInputLines(args, "input-day05.txt")
    val floor = Day05Star2().process(input)
    println(floor)
}

val hasDoublePair: (String) -> Boolean = {
    it
            .zipWithNext()
            .foldIndexed(mutableMapOf<Pair<Char, Char>, MutableList<Int>>()) { index, acc, s ->
                val list = acc.getOrDefault(s, mutableListOf())
                list.add(index)
                acc[s] = list
                acc
            }
            .filter { it.value.size > 1 }
            .values
            .map { it.last() - it.first() }
            .filter { it > 1 }
            .any()
}

val hasRepeatingCharacter: (String) -> Boolean = {
    if (it.length < 3) {
        false
    } else {
        it
                .zip(it.slice(IntRange(2, it.length - 1)))
                .filter { (a, b) -> a == b }
                .any()
    }
}

class Day05Star2 {
    fun process(input: List<String>): Int {
        return input
                .filter(hasDoublePair)
                .filter(hasRepeatingCharacter)
                .count()
    }
}
