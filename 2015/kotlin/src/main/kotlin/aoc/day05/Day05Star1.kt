package aoc.day05

import aoc.readInputLines

fun main(args: Array<String>) {
    val input = readInputLines(args, "input-day05.txt")
    val floor = Day05Star1().process(input)
    println(floor)
}

val has3Vowels: (String) -> Boolean = {
    it
            .filter { it == 'a' || it == 'e' || it == 'i' || it == 'o' || it == 'u' }
            .count() > 2
}

val hasDoubleCharacter: (String) -> Boolean = {
    it
            .zipWithNext()
            .filter { (a, b) -> a == b }
            .any()
}

//ab, cd, pq, or xy
val containBadSequences: (String) -> Boolean = {
    it
            .zipWithNext()
            .filter { (a, b) ->
                a == 'a' && b == 'b'
                        || a == 'c' && b == 'd'
                        || a == 'p' && b == 'q'
                        || a == 'x' && b == 'y'
            }
            .any()
}

class Day05Star1 {
    fun process(input: List<String>) = input
            .filter(hasDoubleCharacter)
            .filter(has3Vowels)
            .filterNot(containBadSequences)
            .count()
}
