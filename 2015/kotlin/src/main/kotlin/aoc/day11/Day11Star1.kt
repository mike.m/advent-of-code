package aoc.day11

import aoc.readInputOneLine

fun main(args: Array<String>) {
    val input = readInputOneLine(args, "input-day10.txt")
    val totalWrapping = Day11Star1().process(input)
    println(totalWrapping)
}

internal class Day11Star1 {
    fun process(input: String): String {
        TODO()
    }
}
