package aoc

import java.io.File

/**
 * Read input from file provided as command line arguments.
 * It is assumed that first argument is file path. Default
 * path is used in case argument isn't provided.
 *
 * @param args Command line arguments.
 * @param defaultPath Default path.
 * @return File content, first line. It is assumed that is's
 * the only line.
 * @throws NoSuchElementException When no suitable line exist.
 */
fun readInputOneLine(args: Array<String>, defaultPath: String) =
        File(getFilePath(args, defaultPath))
                .readLines()
                .first()
                .trim()

/**
 * Read input from file provided as command line arguments.
 * It is assumed that first argument is file path. Default
 * path is used in case argument isn't provided.
 *
 * @param args Command line arguments.
 * @param defaultPath Default path.
 * @return File content as list of strings, one line one element.
 */
fun readInputLines(args: Array<String>, defaultPath: String) =
        File(getFilePath(args, defaultPath))
                .readLines()
                .map { it.trim() }
                .filter { it.isNotEmpty() }
                .toList()

fun getFilePath(args: Array<String>, defaultPath: String) =
        args.getOrElse(0) { defaultPath }
