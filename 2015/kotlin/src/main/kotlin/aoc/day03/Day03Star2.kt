package aoc.day03

import aoc.readInputOneLine

fun main(args: Array<String>) {
    val input = readInputOneLine(args, "input-day03.txt")
    val floor = Day03Star2().process(input)
    println(floor)
}

class Day03Star2 {
    fun process(input: String): Int {
        val actors = listOf(Actor(), Actor())
        val houses = input.mapIndexed { i, c -> actors[i % actors.size].move(c) }.toMutableSet()
        houses.add(Pair(0, 0))
        return houses.size
    }
}

class Actor {
    var position = Pair(0, 0)

    fun move(c: Char): Pair<Int, Int> {
        position = move(position, c)
        return position
    }
}
