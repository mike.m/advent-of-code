package aoc.day03

import aoc.readInputOneLine

fun main(args: Array<String>) {
    val input = readInputOneLine(args, "input-day03.txt")
    val floor = Day03Star1().process(input)
    println(floor)
}

internal fun move(p: Pair<Int, Int>, c: Char) = when (c) {
    '>' -> Pair(p.first + 1, p.second)
    '<' -> Pair(p.first - 1, p.second)
    '^' -> Pair(p.first, p.second + 1)
    else -> Pair(p.first, p.second - 1)
}

class Day03Star1 {
    fun process(input: String): Int {
        val startingPosition = Pair(0, 0)
        var position = startingPosition
        val houses = input.map { position = move(position, it); position }.toMutableSet()
        houses.add(startingPosition)
        return houses.size
    }
}
