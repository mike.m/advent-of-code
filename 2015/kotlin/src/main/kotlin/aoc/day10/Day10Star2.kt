package aoc.day10

import aoc.readInputOneLine

fun main(args: Array<String>) {
    val input = readInputOneLine(args, "input-day10.txt")
    val totalWrapping = Day10Star2().process(input)
    println(totalWrapping)
}

internal class Day10Star2 {
    fun process(input: String): Int {
        return IntRange(1, 50)
                .fold(input) { acc, _ -> lookAndSay(acc) }
                .length
    }
}
