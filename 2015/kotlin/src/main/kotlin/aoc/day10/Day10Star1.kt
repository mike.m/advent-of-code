package aoc.day10

import aoc.readInputOneLine

fun main(args: Array<String>) {
    val input = readInputOneLine(args, "input-day10.txt")
    val totalWrapping = Day10Star1().process(input)
    println(totalWrapping)
}

internal class Day10Star1 {
    fun process(input: String): Int {
        return IntRange(1, 40)
                .fold(input) { acc, _ -> lookAndSay(acc) }
                .length
    }
}

internal fun lookAndSay(input: String) = "$input "
        .zipWithNext()
        .foldIndexed(mutableListOf(0)) { i, acc, c ->
            if (c.first != c.second) acc.add(i + 1)
            acc
        }
        .zipWithNext { left, right -> "${right - left}${input[left]}" }
        .joinToString("")
