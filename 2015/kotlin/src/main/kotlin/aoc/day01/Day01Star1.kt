package aoc.day01

import aoc.readInputOneLine

fun main(args: Array<String>) {
    val input = readInputOneLine(args, "input-day01.txt")
    val floor = Day01Star1().process(input)
    println(floor)
}

fun floorDelta(c: Char) = when (c) {
    '(' -> 1
    ')' -> -1
    else -> throw IllegalStateException("Unexpected character: $c")
}

class Day01Star1 {
    fun process(input: String) = input
            .map { floorDelta(it) }
            .reduce { accumulator, value -> accumulator + value }
}
