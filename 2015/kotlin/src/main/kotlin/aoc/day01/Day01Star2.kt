package aoc.day01

import aoc.readInputOneLine

fun main(args: Array<String>) {
    val input = readInputOneLine(args, "input-day01.txt")
    val floor = Day01Star2().process(input)
    println(floor)
}

class Day01Star2 {
    fun process(input: String): Int {
        var floor = 0
        var firstBasementStep = 0
        for (c in input) {
            val delta = floorDelta(c)
            floor += delta
            firstBasementStep++
            if (floor < 0) {
                break
            }
        }
        return firstBasementStep
    }
}
