package challenges

import java.time.LocalDateTime

fun main(args: Array<String>) {
    println(isPal("a"))
    println(isPal("aa"))
    println(isPal("aba"))
    println(isPal("abba"))
    println(isPal("abcba"))
    println(isPal("abcda"))
    println("---")
    println(LocalDateTime.now())
    println(minPal("aabbaaabbaaacbccbccaaabbzaaabbaaabba"))
    println(LocalDateTime.now())
}

fun minPal(s: String, start: Int = 0): Pair<Int, String> {
    val end = s.length - 1
    var result = Pair(Int.MAX_VALUE, "")
    for (i in start..end) {
        val slice = s.slice(start..i)
        if (isPal(slice)) {
            //println("str($start,$i)='$slice'")
            val temp = if (i == end) {
                Pair(1, s)
            } else {
                val zzz = minPal(s, i + 1)
                Pair(zzz.first + 1, "$slice|${zzz.second}")
            }
            result = if (result.first < temp.first) result else temp
        }
    }
    return result
}

fun isPal(str: String) = str.length == 1 || isPal2(str)

fun isPal2(str: String): Boolean {
    var i = 0
    var j = str.length - 1
    while (i < j) {
        if (str[i] != str[j]) return false
        i++
        j--
    }
    return true
}