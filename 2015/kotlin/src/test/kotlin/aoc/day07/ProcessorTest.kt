package aoc.day07

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ProcessorTest {

    @Test
    fun allGates() {
        //123 -> x
        //456 -> y
        //x AND y -> d
        //x OR y -> e
        //x LSHIFT 2 -> f
        //y RSHIFT 2 -> g
        //NOT x -> h
        //NOT y -> i
        val listOf = listOf(
                "123 -> x",
                "456 -> y",
                "x AND y -> d",
                "x OR y -> e",
                "x LSHIFT 2 -> f",
                "y RSHIFT 2 -> g",
                "NOT x -> h",
                "NOT y -> i"
        )
        val p = Processor(listOf)
        p.execute()
        //d: 72
        //e: 507
        //f: 492
        //g: 114
        //h: 65412
        //i: 65079
        //x: 123
        //y: 456
        Assertions.assertNotNull(p.signals["d"])
        Assertions.assertEquals(72, p.signals["d"]!!.value)
        Assertions.assertNotNull(p.signals["e"])
        Assertions.assertEquals(507, p.signals["e"]!!.value)
        Assertions.assertNotNull(p.signals["f"])
        Assertions.assertEquals(492, p.signals["f"]!!.value)
        Assertions.assertNotNull(p.signals["g"])
        Assertions.assertEquals(114, p.signals["g"]!!.value)
        Assertions.assertNotNull(p.signals["h"])
        Assertions.assertEquals(65412, p.signals["h"]!!.value)
        Assertions.assertNotNull(p.signals["i"])
        Assertions.assertEquals(65079, p.signals["i"]!!.value)
        Assertions.assertNotNull(p.signals["x"])
        Assertions.assertEquals(123, p.signals["x"]!!.value)
        Assertions.assertNotNull(p.signals["y"])
        Assertions.assertEquals(456, p.signals["y"]!!.value)
    }

    @Test
    fun badInput() {
        val e = Assertions.assertThrows(IllegalStateException::class.java) {
            Processor(listOf("bad line"))
        }
        Assertions.assertEquals("Could not parse line: 1", e.message)
    }
}
