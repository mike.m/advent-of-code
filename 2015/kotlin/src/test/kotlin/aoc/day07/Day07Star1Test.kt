package aoc.day07

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class Day07Star1Test {

    val d = Day07Star1()

    @Test
    fun signalAOk() {
        val a = d.process(listOf("1 -> a"))
        Assertions.assertEquals(1, a)
    }

    @Test
    fun signalANotFound() {
        val a = Assertions.assertThrows(IllegalStateException::class.java) { d.process(listOf("1 -> isPal2")) }
        Assertions.assertEquals("Signal 'a' not found", a.message)
    }

    @Test
    fun signalANotInitialized() {
        val a = Assertions.assertThrows(IllegalStateException::class.java) { d.process(listOf("a -> isPal2")) }
        Assertions.assertEquals("Signal 'a' has not been initialized", a.message)
    }
}
