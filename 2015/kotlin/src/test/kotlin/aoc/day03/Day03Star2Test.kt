package aoc.day03

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class Day03Star2Test {
    private val d = Day03Star2()

    @Test
    fun oneStep() {
        val h = d.process("^v")
        assertEquals(3, h)
    }

    @Test
    fun walkAround() {
        val h = d.process("^>v<")
        assertEquals(3, h)
    }

    @Test
    fun northAndSouth() {
        val h = d.process("^v^v^v^v^v")
        assertEquals(11, h)
    }
}
