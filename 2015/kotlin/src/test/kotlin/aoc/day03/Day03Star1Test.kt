package aoc.day03

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class Day03Star1Test {
    private val d = Day03Star1()

    @Test
    fun oneStep() {
        val h = d.process(">")
        assertEquals(2, h)
    }

    @Test
    fun walkAround() {
        val h = d.process("^>v<")
        assertEquals(4, h)
    }

    @Test
    fun northAndSouth() {
        val h = d.process("^v^v^v^v^v")
        assertEquals(2, h)
    }
}
