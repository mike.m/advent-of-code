package aoc.day04

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class Day04Star1Test {
    private val d = Day04Star1()

    @Test
    fun findHash() {
        val i = d.process("abcdef")
        Assertions.assertEquals(609043, i)
    }

    @Test
    fun findAnotherHash() {
        val i = d.process("pqrstuv")
        Assertions.assertEquals(1048970, i)
    }
}
