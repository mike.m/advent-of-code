package aoc.day10

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class Day10Star2Test {
    private val d = Day10Star1()

    @Test
    fun one() {
        Assertions.assertEquals(82350, d.process("1"))
    }
}
