package aoc.day10

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class LookAndSayTest {
    @Test
    fun one() {
        Assertions.assertEquals("11", lookAndSay("1"))
    }

    @Test
    fun oneOne() {
        Assertions.assertEquals("21", lookAndSay("11"))
    }

    @Test
    fun twoOne() {
        Assertions.assertEquals("1211", lookAndSay("21"))
    }

    @Test
    fun oneToOneOne() {
        Assertions.assertEquals("111221", lookAndSay("1211"))
    }

    @Test
    fun oneOneTwoTwoOne() {
        Assertions.assertEquals("312211", lookAndSay("111221"))
    }
}
