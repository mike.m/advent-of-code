package aoc.day06

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class GradientExecutionTest {
    @Test
    fun increaseLights() {
        val size = 6
        val lights = IntArray(size * size) { 0 }
        lights[5 * size + 1] = 1
        val c = Command(Action.TURN_ON, Pair(1, 1), Pair(3, 5))
        executeGradientCommand(lights, c, size)
        Assertions.assertEquals(16, lights.sum())
        val expected = intArrayOf(
                0, 0, 0, 0, 0, 0,
                0, 1, 1, 1, 0, 0,
                0, 1, 1, 1, 0, 0,
                0, 1, 1, 1, 0, 0,
                0, 1, 1, 1, 0, 0,
                0, 2, 1, 1, 0, 0
        )
        Assertions.assertArrayEquals(expected, lights)

    }

    @Test
    fun decreaseLights() {
        val size = 6
        val lights = IntArray(size * size) { 1 }
        lights[5 * size + 1] = 2
        lights[5 * size + 2] = 0
        val c = Command(Action.TURN_OFF, Pair(1, 1), Pair(3, 5))
        executeGradientCommand(lights, c, size)
        Assertions.assertEquals(22, lights.sum())
        val expected = intArrayOf(
                1, 1, 1, 1, 1, 1,
                1, 0, 0, 0, 1, 1,
                1, 0, 0, 0, 1, 1,
                1, 0, 0, 0, 1, 1,
                1, 0, 0, 0, 1, 1,
                1, 1, 0, 0, 1, 1
        )
        Assertions.assertArrayEquals(expected, lights)
    }

    @Test
    fun doubleIncreaseLights() {
        val size = 6
        val lights = IntArray(size * size) { 0 }
        lights[5 * size + 1] = 1
        lights[5 * size + 2] = 2
        val c = Command(Action.TOGGLE, Pair(1, 1), Pair(3, 5))
        executeGradientCommand(lights, c, size)
        Assertions.assertEquals(33, lights.sum())
        val expected = intArrayOf(
                0, 0, 0, 0, 0, 0,
                0, 2, 2, 2, 0, 0,
                0, 2, 2, 2, 0, 0,
                0, 2, 2, 2, 0, 0,
                0, 2, 2, 2, 0, 0,
                0, 3, 4, 2, 0, 0
        )
        Assertions.assertArrayEquals(expected, lights)
    }
}
