package aoc.day06

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ToggleExecutionTest {
    @Test
    fun toggleLightsOn() {
        val size = 6
        val lights = BooleanArray(size * size) { false }
        val c = Command(Action.TURN_ON, Pair(1, 1), Pair(3, 5))
        executeToggleCommand(lights, c, size)
        Assertions.assertEquals(15, lights.count { it })
        val expected = booleanArrayOf(
                false, false, false, false, false, false,
                false, true, true, true, false, false,
                false, true, true, true, false, false,
                false, true, true, true, false, false,
                false, true, true, true, false, false,
                false, true, true, true, false, false
        )
        Assertions.assertArrayEquals(expected, lights)
    }

    @Test
    fun toggleLightsOff() {
        val size = 5
        val lights = BooleanArray(size * size) { true }
        val c = Command(Action.TURN_OFF, Pair(1, 1), Pair(2, 2))
        executeToggleCommand(lights, c, size)
        Assertions.assertEquals(size * size - 4, lights.count { it })
        val expected = booleanArrayOf(
                true, true, true, true, true,
                true, false, false, true, true,
                true, false, false, true, true,
                true, true, true, true, true,
                true, true, true, true, true
        )
        Assertions.assertArrayEquals(expected, lights)
    }

    @Test
    fun toggleLightsOnAndOff() {
        val size = 5
        val lights = BooleanArray(size * size) { false }

        // Enable lights
        val cOn = Command(Action.TURN_ON, Pair(1, 1), Pair(2, 2))
        executeToggleCommand(lights, cOn, size)
        // Toggle lights
        val cToggle = Command(Action.TOGGLE, Pair(2, 2), Pair(3, 3))
        executeToggleCommand(lights, cToggle, size)

        Assertions.assertEquals(6, lights.count { it })
        val expected = booleanArrayOf(
                false, false, false, false, false,
                false, true, true, false, false,
                false, true, false, true, false,
                false, false, true, true, false,
                false, false, false, false, false
        )
        Assertions.assertArrayEquals(expected, lights)
    }
}
