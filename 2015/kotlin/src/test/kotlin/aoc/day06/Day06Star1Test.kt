package aoc.day06

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class Day06Star1Test {
    private val d = Day06Star1(6)

    @Test
    fun countLights() {
        val count = d.process(listOf("turn on 1,0 through 3,1",
                "turn off 2,1 through 3,2",
                "toggle 0,1 through 2,3"))

        // Result
        //
        // .xxx..
        // x.x...
        // xxx...
        // xxx...
        // ......
        // ......

        Assertions.assertEquals(11, count)
    }
}
