package aoc.day08

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class Day08Star2Test {

    private val d = Day08Star2()

    @Test
    fun ok() {
        val r = d.process(listOf(
                """""""",
                """"abc"""",
                """"aaa\"aaa"""",
                """"\x27""""
        ))
        Assertions.assertEquals(19, r)
    }
}
