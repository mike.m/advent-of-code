package aoc.day08

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class EncoderTest {
    @Test
    fun quotesOnly() {
        val l = encodedLength("""""""")
        Assertions.assertEquals(6, l)
    }

    @Test
    fun slashInside() {
        val l = encodedLength(""""aaa\"aaa"""")
        Assertions.assertEquals(16, l)
    }

    @Test
    fun hex() {
        val l = encodedLength(""""\x27"""")
        Assertions.assertEquals(11, l)
    }
}
