package aoc.day08

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class Day08Star1Test {

    private val d = Day08Star1()

    @Test
    fun processLines() {
        val r = d.process(listOf(
                """""""",
                """"abc"""",
                """"aaa\"aaa"""",
                """"\x27""""
        ))
        Assertions.assertEquals(12, r)
    }

    @Test
    fun wrongLine() {
        val e = Assertions.assertThrows(IllegalStateException::class.java) {
            d.process(listOf(""""-""""))
        }
        Assertions.assertEquals("Failed to parse line 1: `\"-\"`", e.message)
    }
}
