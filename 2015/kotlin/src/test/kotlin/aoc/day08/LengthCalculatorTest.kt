package aoc.day08

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class LengthCalculatorTest {

    private val lc = LengthCalculator()

    @Test
    fun noStartingQuote() {
        val e = Assertions.assertThrows(IllegalStateException::class.java) {
            lc.memoryLength("""a"""")
        }
        Assertions.assertEquals("String doesn't start or end with double quote", e.message)
    }

    @Test
    fun noEndingQuote() {
        val e = Assertions.assertThrows(IllegalStateException::class.java) {
            lc.memoryLength(""""a""")
        }
        Assertions.assertEquals("String doesn't start or end with double quote", e.message)
    }

    @Test
    fun wrongEscapedCharacter() {
        val e = Assertions.assertThrows(IllegalStateException::class.java) {
            lc.memoryLength(""""\a"""")
        }
        Assertions.assertEquals("Expected backslash, letter or number: Got a at 3", e.message)
    }

    @Test
    fun wrongFirstHexDigit() {
        val e = Assertions.assertThrows(IllegalStateException::class.java) {
            lc.memoryLength(""""\xza"""")
        }
        Assertions.assertEquals("Expected hex number: Got za at index 4", e.message)
    }

    @Test
    fun wrongSecondHexDigit() {
        val e = Assertions.assertThrows(IllegalStateException::class.java) {
            lc.memoryLength(""""\x0z"""")
        }
        Assertions.assertEquals("Expected hex number: Got 0z at index 4", e.message)
    }

    @Test
    fun hexOk() {
        Assertions.assertEquals(4, lc.memoryLength(""""\x00\x0a\xa0\xaa""""))
    }

    @Test
    fun charOk() {
        Assertions.assertEquals(26, lc.memoryLength(""""abcdefghijklmnopqrstuvwxyz""""))
        Assertions.assertEquals(10, lc.memoryLength(""""0123456789""""))
    }

    @Test
    fun escapesOk() {
        Assertions.assertEquals(3, lc.memoryLength(""""\\\"\\""""))
    }

    @Test
    fun wrongCharacter() {
        val e = Assertions.assertThrows(IllegalStateException::class.java) {
            lc.memoryLength(""""+"""")
        }
        Assertions.assertEquals("Letter or backslash expected: Got + at 2", e.message)
    }
}
