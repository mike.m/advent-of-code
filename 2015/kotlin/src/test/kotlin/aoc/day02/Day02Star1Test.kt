package aoc.day02

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class Day02Star1Test {
    private val d = Day02Star1()

    @Test
    fun bigGift() {
        val wrapping = d.process(listOf("2x3x4"))
        Assertions.assertEquals(58, wrapping)
    }

    @Test
    fun smallGift() {
        val wrapping = d.process(listOf("1x1x10"))
        Assertions.assertEquals(43, wrapping)
    }

    @Test
    fun bothGifts() {
        val wrapping = d.process(listOf("1x1x10", "2x3x4"))
        Assertions.assertEquals(101, wrapping)
    }
}
