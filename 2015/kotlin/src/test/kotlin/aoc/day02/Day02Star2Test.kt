package aoc.day02

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class Day02Star2Test {
    private val d = Day02Star2()

    @Test
    fun bigGift() {
        val wrapping = d.process(listOf("2x3x4"))
        Assertions.assertEquals(34, wrapping)
    }

    @Test
    fun smallGift() {
        val wrapping = d.process(listOf("1x1x10"))
        Assertions.assertEquals(14, wrapping)
    }

    @Test
    fun bothGifts() {
        val wrapping = d.process(listOf("1x1x10", "2x3x4"))
        Assertions.assertEquals(48, wrapping)
    }
}
