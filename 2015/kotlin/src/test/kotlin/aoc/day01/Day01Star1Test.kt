package aoc.day01

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class Day01Star1Test {
    @Test
    fun upAndDown() {
        val floor = Day01Star1().process("()")
        Assertions.assertEquals(0, floor)
    }

    @Test
    fun upAndUp() {
        val floor = Day01Star1().process("((")
        Assertions.assertEquals(2, floor)
    }
}
