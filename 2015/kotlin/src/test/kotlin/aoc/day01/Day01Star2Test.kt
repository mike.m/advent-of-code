package aoc.day01

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class Day01Star2Test {
    @Test
    fun enterBasementAtFirstStep() {
        val step = Day01Star2().process(")")
        Assertions.assertEquals(1, step)
    }

    @Test
    fun enterBasementAtFifthStep() {
        val step = Day01Star2().process("()())")
        Assertions.assertEquals(5, step)
    }
}
