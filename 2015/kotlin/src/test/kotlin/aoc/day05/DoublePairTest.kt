package aoc.day05

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class DoublePairTest {
    @Test
    fun noDoublePairAaa() {
        val r = hasDoublePair("aaa")
        Assertions.assertFalse(r)
    }

    @Test
    fun noDoublePairAbcdef() {
        val r = hasDoublePair("abcdef")
        Assertions.assertFalse(r)
    }

    @Test
    fun doublePairAaaa() {
        val r = hasDoublePair("aaaa")
        Assertions.assertTrue(r)
    }

    @Test
    fun doublePairAazaa() {
        val r = hasDoublePair("aazaa")
        Assertions.assertTrue(r)
    }

    @Test
    fun doublePairAazzzzzaazzzz() {
        val r = hasDoublePair("aazzzzzaazzzz")
        Assertions.assertTrue(r)
    }
}
