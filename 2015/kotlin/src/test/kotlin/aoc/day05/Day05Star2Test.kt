package aoc.day05

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class Day05Star2Test {
    private val d = Day05Star2()

    @Test
    fun nice() {
        val nice = d.process(listOf("zzabazz", "xxyxx", "qjhvhtzxzqqjkmpb"))
        Assertions.assertEquals(3, nice)
    }

    @Test
    fun notNice() {
        val nice = d.process(listOf("uurcxstgmygtbstg", "ieodomkazucvgmuy"))
        Assertions.assertEquals(0, nice)
    }
}
