package aoc.day05

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class BadSequencesTest {
    @Test
    fun noBadSequences() {
        val b = containBadSequences("_______________")
        Assertions.assertFalse(b)
    }

    @Test
    fun hasAbSequence() {
        val b = containBadSequences("__________ab")
        Assertions.assertTrue(b)
    }

    @Test
    fun hasCdSequence() {
        val b = containBadSequences("cd_________")
        Assertions.assertTrue(b)
    }

    @Test
    fun hasPqSequence() {
        val b = containBadSequences("__________pq_________")
        Assertions.assertTrue(b)
    }

    @Test
    fun hasXySequence() {
        val b = containBadSequences("xy")
        Assertions.assertTrue(b)
    }
}
