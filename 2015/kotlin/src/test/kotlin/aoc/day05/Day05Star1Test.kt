package aoc.day05

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class Day05Star1Test {
    private val d = Day05Star1()

    @Test
    fun nice() {
        val nice = d.process(listOf("ugknbfddgicrmopn", "aaa"))
        Assertions.assertEquals(2, nice)
    }

    @Test
    fun notNice() {
        val nice = d.process(listOf(
                // A 3 vowels missing
                "bbbbb",

                // Double pair missing
                "aoiqwe",

                // Bad string present
                "aoiixy"))
        Assertions.assertEquals(0, nice)
    }
}
