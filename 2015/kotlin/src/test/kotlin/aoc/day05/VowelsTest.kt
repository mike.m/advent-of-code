package aoc.day05

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class VowelsTest {
    @Test
    fun onlyThreeAaa() {
        val v = has3Vowels("aaa")
        Assertions.assertTrue(v)
    }

    @Test
    fun onlyThreeEee() {
        val v = has3Vowels("eee")
        Assertions.assertTrue(v)
    }

    @Test
    fun onlyThreeIii() {
        val v = has3Vowels("iii")
        Assertions.assertTrue(v)
    }

    @Test
    fun onlyThreeOoo() {
        val v = has3Vowels("ooo")
        Assertions.assertTrue(v)
    }

    @Test
    fun onlyThreeUuu() {
        val v = has3Vowels("uuu")
        Assertions.assertTrue(v)
    }

    @Test
    fun onlyThoVowels() {
        val v = has3Vowels("uxu")
        Assertions.assertFalse(v)
    }

    @Test
    fun noVowels() {
        val v = has3Vowels("pppwwwqqqnnnmmm")
        Assertions.assertFalse(v)
    }
}
