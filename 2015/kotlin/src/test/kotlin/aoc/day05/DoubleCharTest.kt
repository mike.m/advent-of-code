package aoc.day05

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class DoubleCharTest {
    @Test
    fun hasDoubleChar() {
        val d = hasDoubleCharacter("abcc")
        Assertions.assertTrue(d)
    }

    @Test
    fun hasNoDoubleChar() {
        val d = hasDoubleCharacter("abcefghijklm")
        Assertions.assertFalse(d)
    }
}
