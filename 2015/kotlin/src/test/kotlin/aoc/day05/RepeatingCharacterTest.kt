package aoc.day05

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class RepeatingCharacterTest {
    @Test
    fun repeatingCharAba() {
        val r = hasRepeatingCharacter("aba")
        Assertions.assertTrue(r)
    }

    @Test
    fun repeatingCharQwertyaba() {
        val r = hasRepeatingCharacter("qwertyaba")
        Assertions.assertTrue(r)
    }

    @Test
    fun noRepeatingCharAbba() {
        val r = hasRepeatingCharacter("abba")
        Assertions.assertFalse(r)
    }

    @Test
    fun noRepeatingCharAa() {
        val r = hasRepeatingCharacter("aa")
        Assertions.assertFalse(r)
    }
}