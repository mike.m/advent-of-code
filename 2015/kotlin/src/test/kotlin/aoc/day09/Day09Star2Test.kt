package aoc.day09

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

class Day09Star2Test {
    private val d = Day09Star2()

    @Test
    @DisplayName("Longest distance")
    fun longestDistance() {
        val sd = d.process(listOf(
                "London to Dublin = 464",
                "London to Belfast = 518",
                "Dublin to Belfast = 141"
        ))
        Assertions.assertEquals(982, sd)
    }
}
