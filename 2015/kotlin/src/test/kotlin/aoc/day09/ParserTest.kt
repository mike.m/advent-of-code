package aoc.day09

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

class ParserTest {
    @Test
    @DisplayName("Parse cities")
    fun parseCities() {
        val p = Parser()
        p.addRoutes(listOf(
                "London to Dublin = 464",
                "London to Belfast = 518",
                "Dublin to Belfast = 141"
        ))
        Assertions.assertEquals(3, p.locations.size)
        Assertions.assertEquals(p.locations["London"], false)
        Assertions.assertEquals(p.locations["Dublin"], false)
        Assertions.assertEquals(p.locations["Belfast"], false)
        Assertions.assertIterableEquals(p.routes["London"], listOf(
                Pair("Dublin", 464),
                Pair("Belfast", 518)))
        Assertions.assertIterableEquals(p.routes["Dublin"], listOf(
                Pair("London", 464),
                Pair("Belfast", 141)))
        Assertions.assertIterableEquals(p.routes["Belfast"], listOf(
                Pair("London", 518),
                Pair("Dublin", 141)))
    }

    @Test
    @DisplayName("Wrong line")
    fun wrongLine() {
        val p = Parser()
        val e = Assertions.assertThrows(IllegalStateException::class.java) {
            p.addRoutes(listOf("a to isPal2 = 1", "z"))
        }
        Assertions.assertEquals("Failed to parse line 2", e.message)
        Assertions.assertEquals("Could not parse a string: z", e.cause?.message)
    }
}
