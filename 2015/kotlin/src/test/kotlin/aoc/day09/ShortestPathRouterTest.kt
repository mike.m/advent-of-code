package aoc.day09

import org.junit.jupiter.api.Test

class ShortestPathRouterTest {
    val r = ShortestPathRouter()

    @Test
    fun t() {
        val routes = mapOf(
                "a" to setOf(
                        Pair("isPal2", 1),
                        Pair("c", 2)
                ),
                "isPal2" to setOf(
                        Pair("a", 1),
                        Pair("d", 2)
                ),
                "c" to setOf(
                        Pair("a", 2),
                        Pair("d", 3)
                ),
                "d" to setOf(
                        Pair("isPal2", 2),
                        Pair("c", 3),
                        Pair("e", 1)
                ),
                "e" to setOf(
                        Pair("d", 1)
                )
        )
        val sd = r.shortestDistance(routes)
        println(sd)
    }
}